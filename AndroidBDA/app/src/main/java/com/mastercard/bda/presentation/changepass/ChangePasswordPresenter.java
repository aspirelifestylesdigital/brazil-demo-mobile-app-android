package com.mastercard.bda.presentation.changepass;

import android.content.Context;

import com.api.aspire.domain.usecases.ResetPassword;
import com.mastercard.bda.App;
import com.api.aspire.common.constant.ErrCode;
import com.mastercard.bda.domain.usecases.MapProfileApp;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class ChangePasswordPresenter implements ChangePassword.Presenter {

    private CompositeDisposable compositeDisposable;
    private ChangePassword.View view;
    private ResetPassword changePassword;

    ChangePasswordPresenter(Context c) {
        compositeDisposable = new CompositeDisposable();
        this.changePassword = new ResetPassword(c, new MapProfileApp());
    }

    @Override
    public void attach(ChangePassword.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    @Override
    public void doChangePassword(String oldPassword, String newPassword) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        ResetPassword.Params params = new ResetPassword.Params(oldPassword, newPassword);
        compositeDisposable.add(
                changePassword.buildUseCaseCompletable(params)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableCompletableObserver() {
                            @Override
                            public void onComplete() {
                                if (view != null) {
                                    view.dismissProgressDialog();
                                    view.showSuccessMessage();
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                if (view != null) {
                                    view.dismissProgressDialog();
                                    view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
                                }
                            }
                        })
        );

    }

    @Override
    public void abort() {
        compositeDisposable.clear();
    }
}
