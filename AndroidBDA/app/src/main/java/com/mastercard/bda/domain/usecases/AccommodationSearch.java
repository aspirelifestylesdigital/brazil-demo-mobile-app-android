package com.mastercard.bda.domain.usecases;

import android.text.TextUtils;

import com.mastercard.bda.App;
import com.mastercard.bda.R;
import com.mastercard.bda.common.constant.AppConstant;
import com.mastercard.bda.common.constant.CityData;
import com.mastercard.bda.datalayer.datasource.AppGeoCoder;
import com.mastercard.bda.datalayer.entity.SearchContent;
import com.mastercard.bda.domain.mapper.CCACategoriesMapper;
import com.mastercard.bda.domain.mapper.FilterMapper;
import com.mastercard.bda.domain.model.explore.ExploreRView;
import com.mastercard.bda.domain.model.explore.ExploreRViewItem;
import com.mastercard.bda.domain.model.explore.SearchDetailItem;
import com.mastercard.bda.domain.repository.B2CRepository;
import com.mastercard.bda.presentation.explore.DiningSortingCriteria;
import com.mastercard.bda.presentation.explore.ExplorePresenter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 6/23/2017.
 */

public class AccommodationSearch extends UseCase<AccommodationSearch.SearchResult, AccommodationSearch.Params> {

    private B2CRepository b2CRepository;
    private AppGeoCoder geoCoder;
    private List<SearchContent> data;

    public AccommodationSearch(B2CRepository b2CRepository, AppGeoCoder appGeoCoder) {
        this.b2CRepository = b2CRepository;
        this.geoCoder = appGeoCoder;
        data = new ArrayList<>();
    }

    @Override
    Observable<AccommodationSearch.SearchResult> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    public Single<AccommodationSearch.SearchResult> buildUseCaseSingle(Params params) {
        if(params.paging == ExplorePresenter.DEFAULT_PAGE) data.clear();
        return b2CRepository.searchByTerm(params.term, params.paging, params.cities)
                .map(searchContents -> {
                    int count = searchContents.size();
                    removeCityGuideItems(searchContents);
                    if(params.diningCategory){
                        filterIfCategoryIsDining(searchContents);
                    }

                    filterGalleryItem(searchContents);
                    FilterMapper.handleCCASubGeoRegionSearch(searchContents);

                    filter(searchContents, params.category);
                    if(params.withOffers) filterData(searchContents);
                    int length = data.size();
                    data.addAll(searchContents);
                    return new SearchResult(viewData(length, searchContents, params.diningCategory), count == 0);
                });
    }
    private void filter(List<SearchContent> originalData, String originalCategoryName){
        removeId36(originalData);
        filterTour(originalData, originalCategoryName);
        filterTravel(originalData, originalCategoryName);
        //filterVipTravelService(originalData, originalCategoryName);
        filterEntertainment(originalData,originalCategoryName);
        //removeGolf(originalData);
        removeCruise(originalData);
        removeGolfMerchandise(originalData);
        FilterMapper.filterTransportationSearch(originalData, originalCategoryName);
        FilterMapper.filterSpecialtyTravelSearch(originalData);
    }
    private void removeId36(List<SearchContent> originalData){
        Iterator<SearchContent> iterator = originalData.iterator();
        while (iterator.hasNext()){
            SearchContent tiles = iterator.next();
            if(tiles.ID() == 36){
                iterator.remove();
                break;
            }
        }
    }
    private void filterTour(List<SearchContent> originalData, String originalCategoryName){
        if(originalCategoryName != null && originalCategoryName.equalsIgnoreCase("tours")){
            Iterator<SearchContent> iterator = originalData.iterator();
            while (iterator.hasNext()){
                SearchContent tiles = iterator.next();
                if(!"Sightseeing".equalsIgnoreCase(tiles.subCategory())){
                    iterator.remove();
                }
            }
        }
    }
    private void filterTravel(List<SearchContent> originalData, String originalCategoryName){
        if(originalCategoryName != null && originalCategoryName.equalsIgnoreCase("travel")){
            Iterator<SearchContent> iterator = originalData.iterator();
            while (iterator.hasNext()){
                SearchContent tiles = iterator.next();
                if(!("Private Jet Travel".equalsIgnoreCase(tiles.category()) ||
                        "Airport Services".equalsIgnoreCase(tiles.subCategory()) ||
                        "Travel Services".equalsIgnoreCase(tiles.subCategory()))){
                    iterator.remove();
                }
            }
        }
    }
    private void filterVipTravelService(List<SearchContent> originalData, String originalCategoryName){
        if(originalCategoryName != null){
            AppConstant.EXPLORE_CATEGORY vipTravelService = null;
            if(originalCategoryName.equalsIgnoreCase("tours")){
                vipTravelService = AppConstant.EXPLORE_CATEGORY.TOUR_SUBCAT;
            }else if(originalCategoryName.equalsIgnoreCase("airport services")){
                vipTravelService = AppConstant.EXPLORE_CATEGORY.AIRPORT_SERVICES_SUBCAT;
            }else if(originalCategoryName.equalsIgnoreCase("travel services")){
                vipTravelService = AppConstant.EXPLORE_CATEGORY.TRAVEL_SERVICES_SUBCAT;
            }

            if(vipTravelService != null){
                Iterator<SearchContent> tilesIterator = originalData.iterator();
                while (tilesIterator.hasNext()){
                    SearchContent tiles = tilesIterator.next();
                    String subCateItem = tiles.subCategory();
                    if(!vipTravelService.getValue().equalsIgnoreCase(subCateItem)){
                        tilesIterator.remove();
                    }
                }
            }
        }
    }
    private void filterEntertainment(List<SearchContent> originalData, String originalCategoryName){
        if(originalCategoryName != null && originalCategoryName.equalsIgnoreCase("entertainment")){
            Iterator<SearchContent> iterator = originalData.iterator();
            while (iterator.hasNext()){
                SearchContent tiles = iterator.next();
                if(!("Tickets".equalsIgnoreCase(tiles.category()) || "Entertainment Experiences".equalsIgnoreCase(tiles.subCategory()) ||
                        "Major Sports Events".equalsIgnoreCase(tiles.subCategory()))){
                    iterator.remove();
                }
            }
        }
    }
    private void removeGolf(List<SearchContent> originalData){
        Iterator<SearchContent> iterator = originalData.iterator();
        while (iterator.hasNext()){
            SearchContent tiles = iterator.next();
            if("Golf".equalsIgnoreCase(tiles.category()) ||
                    ("Specialty Travel".equalsIgnoreCase(tiles.category()) && "Golf Experiences".equalsIgnoreCase(tiles.subCategory()))){
                iterator.remove();
            }
        }
    }

    private void removeGolfMerchandise(List<SearchContent> originalData) {
        Iterator<SearchContent> iterator = originalData.iterator();
        while (iterator.hasNext()){
            SearchContent tiles = iterator.next();
            if("Golf Merchandise".equalsIgnoreCase(tiles.category())){
                iterator.remove();
            }
        }
    }
    private void removeCruise(List<SearchContent> originalData){
        Iterator<SearchContent> iterator = originalData.iterator();
        while (iterator.hasNext()){
            SearchContent tiles = iterator.next();
            if("Cruises".equalsIgnoreCase(tiles.category()) && TextUtils.isEmpty(tiles.subCategory())){
                iterator.remove();
            }
        }
    }
    private void filterGalleryItem(List<SearchContent> originalData){
        List<SearchContent> toBeRemoved = new ArrayList<>();
        for (SearchContent tiles: originalData) {
            if(tiles.product().equalsIgnoreCase("CCA") && tiles.description().equalsIgnoreCase("..."))
                toBeRemoved.add(tiles);
        }
        originalData.removeAll(toBeRemoved);
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    private void removeCityGuideItems(List<SearchContent> originalData) {
        Iterator<SearchContent> iterator = originalData.iterator();
        while (iterator.hasNext()){
            SearchContent searchContent = iterator.next();
            if(CityData.isCityGuideItem(searchContent.secondaryID())) {
                iterator.remove();
            }
        }
    }

    private void filterIfCategoryIsDining(List<SearchContent> originalData) {
        /*List<Integer> ccaDiningIDs = new CCACategoriesMapper(CCACategoriesMapper.WHICH.ID).ids("dining");
        List<SearchContent> toBeRemoved = new ArrayList<>();
        for (SearchContent content: originalData) {
            if(!(content.secondaryID() == CityData.diningCode() || ccaDiningIDs.contains(content.ID()))) {
                toBeRemoved.add(content);
            }
        }
        originalData.removeAll(toBeRemoved);*/
        Iterator<SearchContent> iterator = originalData.iterator();
        while (iterator.hasNext()){
            SearchContent searchContent = iterator.next();
            if(!("IA".equalsIgnoreCase(searchContent.product()) ||
                    "Dining".equalsIgnoreCase(searchContent.category()) ||
                    "Culinary Experiences".equalsIgnoreCase(searchContent.subCategory()))){
                iterator.remove();
            }
        }
    }
/*
    private void filterCCARegions(List<SearchContent> originalData) {
        //-- Fix #UDA-203
        //List<Integer> checkIDs = CityData.TitleContentIDs;
        List<SearchContent> toBeRemoved = new ArrayList<>();
        String[] geographicRegions = CityData.geographicRegion().split("\\.");
        for (SearchContent tiles: originalData) {
            //if(!checkIDs.contains(tiles.ID())) continue;
            if(TextUtils.isEmpty(tiles.subCategory()) || "Global Partners".equals(tiles.subCategory())) continue;
            if(!"Hotels".equalsIgnoreCase(tiles.category()) &&
                    !"Vacation Packages".equalsIgnoreCase(tiles.category()) &&
                    !"Cruises".equalsIgnoreCase(tiles.category())) continue;
            if(shouldIgnore(CityData.isCity(), geographicRegions, tiles)) toBeRemoved.add(tiles);
        }
        originalData.removeAll(toBeRemoved);
    }
*/

    private void filterData(List<SearchContent> originalData) {
        List<SearchContent> toBeRemoved = new ArrayList<>();
        for (SearchContent content: originalData) {
            if(!content.hasOffer())
                toBeRemoved.add(content);
        }
        originalData.removeAll(toBeRemoved);
    }

    private List<ExploreRViewItem> viewData(int startIndex, List<SearchContent> dataList, boolean diningItem) {
        CCACategoriesMapper ccaCategoriesMapper = new CCACategoriesMapper();
        final List<ExploreRViewItem> exploreRViewList = new ArrayList<>();
        int length = dataList.size();
        for (int i = 0; i < length; i++) {
            SearchContent content = dataList.get(i);
            final ExploreRViewItem exploreRViewItem = viewDatum(startIndex + i, content, diningItem, ccaCategoriesMapper);
            exploreRViewList.add(exploreRViewItem);
        }
        return exploreRViewList;
    }

    private ExploreRViewItem viewDatum(int dataIndex, SearchContent content, boolean diningItem, CCACategoriesMapper ccaMapper) {
        String description;
        if(diningItem || "IA".equals(content.product())) {
            description = TextUtils.isEmpty(content.address3()) ? " " : content.address3();
        } else if(ccaMapper.multipleCities(content.ID())) {
            description = App.getInstance().getString(R.string.text_multiple_cities);
        } else {
            description = " ";
        }
        ExploreRViewItem exploreRViewItem = new ExploreRViewItem(
                content.ID(),
                content.title().trim(),
                description,
                null,
                content.hasOffer(),
                content.description().replace("&nbsp;",""),
                dataIndex
        );
        exploreRViewItem.setItemType(ExploreRView.ItemType.SEARCH);
        if(diningItem) {
            exploreRViewItem.setSortingCriteria(new DiningSortingCriteria(
                    geoCoder.getFullGeoCoder(content.address()),
                    content.cuisine()
            ));
        }
        return exploreRViewItem;
    }

    public SearchDetailItem getItemView(int index) {
        SearchContent searchContent = data.get(index);

        SearchDetailItem item = new SearchDetailItem(
                searchContent.ID(),
                searchContent.secondaryID(),
                searchContent.title(),
                searchContent.product()
        );
        if("IA".equalsIgnoreCase(searchContent.product()) ||
                "Dining".equalsIgnoreCase(searchContent.category())){
            item.setItemType(ExploreRView.ItemType.DINING);
        } else {
            item.setItemType(ExploreRView.ItemType.NORMAL);
        }

        return item;
    }

    private boolean shouldIgnore(boolean city, String[] geographicRegions, SearchContent tiles) {
        String geographicRegion = tiles.geographicRegion();
        String subCategory = tiles.subCategory();
        if(geographicRegions.length== 0 || geographicRegions[0].equals("")) return true;
        if(!subCategory.contains(geographicRegions[0])) return true;
        if(!city && geographicRegions.length == 1) return false;
        for (String region : geographicRegions) {
            if(geographicRegion.contains(region)) return false;
        }
        return true;
    }

    public static class Params {
        private final String term;
        private final Integer paging;
        private final String[] cities;
        private final Boolean withOffers;
        private final Boolean diningCategory;
        private final String category;
        public Params(String term, Integer paging, Boolean withOffers, String city, Boolean dining, String category) {
            this.term = term;
            this.paging = paging;
            this.cities = new String[]{city};
            this.withOffers = withOffers;
            this.diningCategory = dining;
            this.category = category;
        }
    }

    public static class SearchResult {
        public final List<ExploreRViewItem> exploreRViewItems;
        public final boolean reachEnd;

        public SearchResult(List<ExploreRViewItem> exploreRViewItems, boolean reachEnd) {
            this.exploreRViewItems = exploreRViewItems;
            this.reachEnd = reachEnd;
        }
    }
}
