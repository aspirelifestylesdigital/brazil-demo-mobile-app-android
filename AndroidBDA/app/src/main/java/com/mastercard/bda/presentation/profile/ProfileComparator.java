package com.mastercard.bda.presentation.profile;

import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import com.mastercard.bda.domain.model.Profile;
import com.mastercard.bda.presentation.widget.CustomSpinner;

/**
 * Created by vinh.trinh on 7/11/2017.
 */

class ProfileComparator {

    Profile original;

    private EditText edtFirstName, edtLastName, edtEmail, edtPhone;
    private SwitchCompat locationSwitch;
    /*private Spinner salutationSpinner;*/
    private CustomSpinner salutationSpinner;
    private Button btnSubmit, btnCancel;
    private boolean firstName, lastName, email, phone, salutation;

    //<editor-fold desc="variable TextWatcher">
    private TextWatcher mFirstNameTextWatcher = new TextWatcher() {
        @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
        @Override
        public void afterTextChanged(Editable s) {
            if(edtFirstName.isFocused()) {
                String string = s.toString();
                firstName = !string.equals(original.getFirstName());
                onChanged();
            }
        }
    };
    private TextWatcher mLastNameTextWatcher = new TextWatcher() {
        @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
        @Override
        public void afterTextChanged(Editable s) {
            if(edtLastName.isFocused()) {
                String string = s.toString();
                lastName = !string.equals(original.getLastName());
                onChanged();
            }
        }
    };
    private TextWatcher mPhoneTextWatcher = new TextWatcher() {
        @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
        @Override
        public void afterTextChanged(Editable s) {
            if(edtPhone.isFocused()) {
                    /*String string = s.toString();
                    if(string.indexOf("+") == 0){
                        if(string.length() > 1) {
                            string = string.substring(1);
                        }else{
                            string = "";
                        }
                    }*/
                String text = s.toString();
                if (text.length() == 1 || text.length() == Profile.PHONE_MAX_LENGTH) {
                    int maxLength = (text.charAt(0) == '+') ? Profile.PHONE_LENGTH_ACCEPT : Profile.PHONE_MAX_LENGTH;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    edtPhone.setFilters(fArray);
                }

                String originalPhone = original.getPhone().replace("+", "");
                phone = !s.toString().replace("+", "").equals(originalPhone);
                onChanged();
            }
        }
    };
    //</editor-fold>

    ProfileComparator(EditText edtFirstName, EditText edtLastName, EditText edtEmail, EditText edtPhone,
                      Button btnSubmit, Button btnCancel, SwitchCompat locationSwitch, CustomSpinner salutationSpinner) {
        this.edtFirstName = edtFirstName;
        this.edtLastName = edtLastName;
        this.edtEmail = edtEmail;
        this.edtPhone = edtPhone;
        this.btnSubmit = btnSubmit;
        this.btnCancel = btnCancel;
        this.locationSwitch = locationSwitch;
        this.salutationSpinner = salutationSpinner;
        setUp();
    }

    private void setUp() {
        edtFirstName.addTextChangedListener(mFirstNameTextWatcher);
        edtLastName.addTextChangedListener(mLastNameTextWatcher);
      /*  edtEmail.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if(string.contains("*")) return;
                email = !string.equals(original.getEmail());
                onChanged();
            }
        });*/
        edtPhone.addTextChangedListener(mPhoneTextWatcher);
        salutationSpinner.setmSpinnerListener(new CustomSpinner.SpinnerListener() {
            @Override
            public void onArchorViewClick() {

            }

            @Override
            public void onItemSelected(final int index) {
                salutation = !(salutationSpinner.getDropdownAdapter().getItem(index)).equals(original.getSalutation());
                onChanged();

            }
        });
      /*  salutationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                salutation = !(salutationSpinner.getSelectedItem()).equals(original.getSalutation());
                onChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/
    }

    public void removeTextChangeListener(){
        edtFirstName.removeTextChangedListener(mFirstNameTextWatcher);
        edtLastName.removeTextChangedListener(mLastNameTextWatcher);
        edtPhone.removeTextChangedListener(mPhoneTextWatcher);
    }

    public void addTextChangeListener(){
        edtFirstName.addTextChangedListener(mFirstNameTextWatcher);
        edtLastName.addTextChangedListener(mLastNameTextWatcher);
        edtPhone.addTextChangedListener(mPhoneTextWatcher);
    }

    void onChanged() {
        boolean changed = firstName || lastName || phone || email || salutation ||
                locationSwitch.isChecked() == original.isLocationOn();
        btnSubmit.setEnabled(changed);
        btnSubmit.setClickable(changed);
        btnCancel.setEnabled(changed);
        btnCancel.setClickable(changed);
    }
}
