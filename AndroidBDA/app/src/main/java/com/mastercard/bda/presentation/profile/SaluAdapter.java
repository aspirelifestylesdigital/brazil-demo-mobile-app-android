package com.mastercard.bda.presentation.profile;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;

import com.mastercard.bda.R;
import com.support.mylibrary.widget.LetterSpacingTextView;

import java.util.Arrays;
import java.util.List;

/**
 * Created by motdi on 03/08/2017.
 */

public class SaluAdapter extends BaseAdapter implements SpinnerAdapter {

    private List<String> asr;
    private LayoutInflater inflater;

    SaluAdapter(Context ctx, String[] asr) {
        this.asr = Arrays.asList(asr);
        inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    int positionOfValue(String val) {
        return asr.indexOf(val);
    }

    public int getCount() {
        return asr.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getViewTypeCount() {
        return super.getViewTypeCount();
    }

    @Override
    public Object getItem(int i) {
        return asr.get(i);
    }

    @Override
    public long getItemId(int i) {
        return (long) i;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomDropView(position, convertView, parent);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewgroup) {
        return getCustomView(i, view, viewgroup);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(R.layout.preference_spinner_item, parent, false);

        LetterSpacingTextView txt = row.findViewById(R.id.title);
        View view = row.findViewById(R.id.view);
        view.setVisibility(View.GONE);
        txt.setGravity(Gravity.START|Gravity.CENTER_VERTICAL);
        txt.setMaxLines(1);
        txt.setEllipsize(TextUtils.TruncateAt.END);
      /*  if (position == 0) {
            txt.setText(asr.get(0));
            txt.setTextColor(Color.parseColor("#99A1A8"));
            notifyDataSetChanged();
        } else {*/
            txt.setText(asr.get(position));
            txt.setTextColor(Color.parseColor("#011627"));
      //  }

        return row;
    }

    private View getCustomDropView(int position, View convertView, ViewGroup parent) {

        View row = inflater.inflate(R.layout.preference_spinner_item_dropdown, parent, false);

        LetterSpacingTextView txt = row.findViewById(R.id.title);
        View view = row.findViewById(R.id.view);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setTextColor(Color.parseColor("#011627"));

       /* if (position == 0) {
            txt.setText(row.getContext().getString(R.string.text_none));
        } else {*/
            txt.setText(asr.get(position));
            if (position == asr.size() - 1) {
                view.setVisibility(View.GONE);
            }
      //  }

        return row;
    }
}