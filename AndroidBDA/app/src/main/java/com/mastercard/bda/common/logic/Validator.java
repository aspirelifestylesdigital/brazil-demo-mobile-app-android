package com.mastercard.bda.common.logic;

import com.mastercard.bda.common.constant.CountryCode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vinh.trinh on 4/28/2017.
 */

public final class Validator {
    public final int NAME_MIN_LENGTH = 2;
    public final int NAME_MAX_LENGTH = 25;
    public final int PHONE_MAX_LENGTH = 19; /* Profile.PHONE_MAX_LENGTH - 1 */

    private final String EMAIL_REGEX = "^[A-Z0-9a-z\\._%+-]+@{1}([A-Za-z0-9-]+\\.)+[A-Za-z]{2,}$";

    public boolean email(String email) {
        boolean isEmailValid = email.matches(EMAIL_REGEX);
        if(isEmailValid){
            if(email.contains(".")) {
                String passEmail = email.substring(email.lastIndexOf("."));
                if(passEmail.length()>4){
                    isEmailValid = false;
                }
            }
        }
        return isEmailValid ;
    }

    public boolean phone(String phone) {//length?
        return phone.length() == PHONE_MAX_LENGTH;
    }

    public boolean name(String name) {
        return name.length() >= NAME_MIN_LENGTH;
    }

    public boolean password(String pwd, String confirmPwd) {
        return pwd.length() > 0 && confirmPwd.equals(pwd);
    }
    public boolean secretValidator(String secretWord) {
        Pattern pattern;
        Matcher matcher;
        if(secretWord!=null && secretWord.trim().length()>0) {
            final String SECRET_PATTERN =
                    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*\\[\\]\"\\';:_\\-<>\\., =\\+\\/\\\\]).{10,25}";
            pattern = Pattern.compile(SECRET_PATTERN);
            matcher = pattern.matcher(secretWord);
            return matcher.matches();
        } else {
          return false;
        }
    }

    public boolean specialChars(String specialChars){
        Pattern pattern;
        Matcher matcher;
        if(specialChars!=null ) {
            final  String SECRET_PATTERN =
                    "^['a-zA-Z0-9-\\s]*$";
            pattern = Pattern.compile(SECRET_PATTERN);
            matcher = pattern.matcher(specialChars);
            return matcher.matches();
        } else {
            return false;
        }

    }

    public boolean phoneFormatValidator(String phoneNumber) {
        Pattern pattern;
        Matcher matcher;
        if(phoneNumber!=null && phoneNumber.trim().length()>0) {
            final String PHONE_PATTERN = "^\\+?\\d+";
                    //"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*\\[\\]\"\\';:_\\-<>\\., =\\+\\/\\\\]).{10,25}";
            pattern = Pattern.compile(PHONE_PATTERN);
            matcher = pattern.matcher(phoneNumber);
            return matcher.matches();
        } else {
            return false;
        }
    }
    public String findCountryCodeFromScratch(String val) {
        int length = val.length() < 4 ? val.length() : 4;
        for (int i = 1; i <= length; i++) {
            String countryCode = val.substring(0, i);
            if (CountryCode.contains(countryCode)) return countryCode;
        }
        return "";
    }

}
