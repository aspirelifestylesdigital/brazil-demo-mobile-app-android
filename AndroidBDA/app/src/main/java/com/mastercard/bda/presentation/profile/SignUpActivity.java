package com.mastercard.bda.presentation.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.mastercard.bda.App;
import com.mastercard.bda.R;
import com.mastercard.bda.common.GPSChecker;
import com.mastercard.bda.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.mastercard.bda.domain.model.Profile;
import com.mastercard.bda.presentation.LocationPermissionHandler;
import com.mastercard.bda.presentation.base.BaseActivity;
import com.mastercard.bda.presentation.checkout.PassCodeCheckout;
import com.mastercard.bda.presentation.checkout.PassCodeCheckoutPresenter;
import com.mastercard.bda.presentation.checkout.SignInActivity;
import com.mastercard.bda.presentation.home.HomeActivity;
import com.mastercard.bda.presentation.widget.DialogHelper;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public class SignUpActivity extends BaseActivity implements ProfileFragment.ProfileEventsListener, CreateProfile.View
        , PassCodeCheckout.View {

    private CreateProfilePresenter presenter;
    private PassCodeCheckoutPresenter passCodeCheckoutPresenter;
    private DialogHelper dialogHelper;
    private View view;
    ProfileFragment fragment;
    LocationPermissionHandler locationPermissionHandler = new LocationPermissionHandler();

    private CreateProfilePresenter presenter() {
        return new CreateProfilePresenter(getApplicationContext());
    }

    private PassCodeCheckoutPresenter buildPassCodePresenter() {
        return new PassCodeCheckoutPresenter(getApplicationContext());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_fragment_holder);
        view = findViewById(R.id.fragment_place_holder);
        presenter = presenter();
        passCodeCheckoutPresenter = buildPassCodePresenter();
        passCodeCheckoutPresenter.attach(this);
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        if (savedInstanceState == null) {
            fragment = ProfileFragment.newInstance(ProfileFragment.PROFILE.CREATE);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder,
                            fragment,
                            ProfileFragment.class.getSimpleName())
                    .commit();
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SIGN_UP.getValue());
        }
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void onCheckPassCodeSubmit(String passCode) {
        passCodeCheckoutPresenter.checkoutPossCode(passCode);
    }

    @Override
    public void onProfileSubmit(Profile profile, String password, String passCode) {
        presenter.createProfile(profile, password, passCode);
    }

    @Override
    public void showProfileCreatedDialog() {
        dialogHelper.alert(null, getString(R.string.profile_created_message), dialog -> proceedToHome());
    }

    private void proceedToHome() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();

        // Track GA with "Sign up" event
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                AppConstant.GA_TRACKING_LABEL.SIGN_UP.getValue());
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (!dialogHelper.networkUnavailability(errCode, extraMsg)) {
            if (ErrCode.USER_EXISTS_ERROR.name().equalsIgnoreCase(extraMsg)) {
                //error exist email
                dialogHelper.alert(
                        getString(R.string.dialogTitleError),
                        getResources().getString(R.string.errorAccountEmailExists),
                        dialog -> {
                            if (fragment != null && fragment.getView() != null) {
                                fragment.getView().postDelayed(() -> fragment.showHighlightFieldEmail(), 100);
                            }
                        });
            } else {
                dialogHelper.showGeneralError();
            }
        }
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void onCheckPassCodeSuccessfully() {
        fragment.ifValidedPassCode();
    }

    @Override
    public void onCheckPassCodeFailure() {
//        dialogHelper.alert("ERROR!", getString(R.string.ErrorPassCodeEnterValid), dialog -> {
//            if(fragment != null) {
//                fragment.getView().postDelayed(() -> fragment.invokeSoftKey(), 100);
//            }
//        });

        dialogHelper.action(App.getInstance().getString(R.string.dialogTitleError), getString(R.string.ErrorPassCodeEnterValid), getString(R.string.text_ok), getString(R.string.text_cancel),
                (dialogInterface, i) -> invokeError(),
                (dialogInterface, i) -> toSignInScreen());
        fragment.onCheckPasscodeFail();

    }

    private void invokeError() {
        if (fragment != null) {
            fragment.getView().postDelayed(() -> fragment.invokeSoftKey(), 100);
        }
    }

    private void toSignInScreen() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void onProfileCancel() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        onBackPressed();
    }

    @Override
    public void onProfileInputError(String message) {
        dialogHelper.profileDialog(message, dialog -> {
            if (fragment != null) {
                fragment.getView().postDelayed(() -> fragment.invokeSoftKey(), 100);
            }
        });
    }

    @Override
    public void onFragmentCreated() {
    }

    @Override
    public void onLocationSwitchOn() {
        if (!locationPermissionHandler.hasPermission(this)) {
            locationPermissionHandler.requestPermission(this);
        } else {
            if (!GPSChecker.GPSEnable(getApplicationContext())) {
                dialogHelper.action(null, getApplicationContext().getString(R.string.dialogLocationContent),
                        getApplicationContext().getString(R.string.textSetting), getApplicationContext().getString(R.string.text_cancel),
                        (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean granted = locationPermissionHandler.handlingPermissionResult(requestCode, grantResults);
        if (!granted) fragment.switchOff();
        else if (!GPSChecker.GPSEnable(getApplicationContext())) {
            dialogHelper.action(null, getApplicationContext().getString(R.string.dialogLocationContent),
                    getApplicationContext().getString(R.string.textSetting), getApplicationContext().getString(R.string.text_cancel),
                    (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();
    }

    @Override
    public void onCheckPassCodeNone() {

    }

    @Override
    public void updatePassCodeApiCompleted() {
        //-- dummy, don't use here
    }

    @Override
    public void onErrorGetToken() {
        if (dialogHelper != null) {
            dialogHelper.showGetTokenError();
        }
    }
}
