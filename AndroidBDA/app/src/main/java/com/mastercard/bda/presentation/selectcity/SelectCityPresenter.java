package com.mastercard.bda.presentation.selectcity;

import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.usecases.SaveProfile;
import com.mastercard.bda.App;
import com.mastercard.bda.common.constant.CityData;
import com.mastercard.bda.domain.mapper.profile.ProfileLogic;
import com.mastercard.bda.domain.usecases.MapProfileApp;

import org.json.JSONException;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SelectCityPresenter implements SelectCity.Presenter {

    private SelectCity.View view;

    private SaveProfile saveProfile;
    private PreferencesStorageAspire preferencesStorage;

    public SelectCityPresenter(Context c) {

        this.preferencesStorage = new PreferencesStorageAspire(c);
        this.saveProfile = new SaveProfile(c, new MapProfileApp());
    }

    @Override
    public void attach(SelectCity.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        this.view = null;
    }

    @Override
    public void saveSelectCity(final String cityNameVal) {
        if (TextUtils.isEmpty(cityNameVal)) {
            return;
        }
        //save local
        ProfileAspire profile = null;

        if (CityData.setSelectedCity(cityNameVal)) {
            PreferencesStorageAspire.Editor editor = preferencesStorage.editor();
            try {
                profile = preferencesStorage.profile();
                //update new city
                ProfileLogic.setAppUserPreference(profile.getAppUserPreferences(), ConstantAspireApi.APP_USER_PREFERENCES.SELECTED_CITY_KEY, cityNameVal);
                editor.profile(profile);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            editor.selectedCity(cityNameVal).build().saveAsync();
        }

        //save remote - don't stuck here. can not save city in Remote will save when upload Profile
        if (!App.getInstance().hasNetworkConnection()) {
            return;
        }
        //run background
        if (profile != null) {
            saveProfile.saveProfileSelectCity(profile)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        }
    }

}
