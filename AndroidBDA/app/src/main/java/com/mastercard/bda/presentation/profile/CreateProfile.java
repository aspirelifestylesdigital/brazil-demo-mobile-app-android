package com.mastercard.bda.presentation.profile;

import com.api.aspire.common.constant.ErrCode;
import com.mastercard.bda.domain.model.Profile;
import com.mastercard.bda.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public interface CreateProfile {

    interface View {
        void showProfileCreatedDialog();
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
    }

    interface Presenter extends BasePresenter<View> {
        void createProfile(Profile profile, String password,String passCode);
        void abort();
    }

}
