package com.mastercard.bda.datalayer.entity.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 7/24/2017.
 */

public class ForgotPasswordRequest {

    @Expose
    @SerializedName("ConsumerKey")
    private final String consumerKey;
    @Expose
    @SerializedName("Functionality")
    private final String functionality;
    @Expose
    @SerializedName("Email2")
    private final String email;

    public ForgotPasswordRequest(String email) {
        this.consumerKey = "DMA";
        this.functionality = "ForgotPassword";
        this.email = email;
    }
}
