package com.mastercard.bda.presentation.checkout;

import com.api.aspire.common.constant.ErrCode;
import com.mastercard.bda.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public interface SignIn {

    interface View {
        void proceedToHome();
        void onCheckPassCodeFailure();
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
    }

    interface Presenter extends BasePresenter<View> {
        void doSignIn(String email, String password);
        void abort();
    }

}
