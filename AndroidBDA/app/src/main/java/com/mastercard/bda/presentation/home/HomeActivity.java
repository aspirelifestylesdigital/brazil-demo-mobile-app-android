package com.mastercard.bda.presentation.home;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.google.android.gms.tasks.OnSuccessListener;
import com.mastercard.bda.App;
import com.mastercard.bda.R;
import com.mastercard.bda.common.constant.AppConstant;
import com.mastercard.bda.common.constant.CityData;
import com.mastercard.bda.common.constant.IntentConstant;
import com.mastercard.bda.common.constant.RequestCode;
import com.mastercard.bda.common.constant.ResultCode;
import com.mastercard.bda.presentation.base.NavDrawerActivity;
import com.mastercard.bda.presentation.explore.ExploreFragment;
import com.mastercard.bda.presentation.inspiregallery.InspireGalleryFragment;
import com.mastercard.bda.presentation.selectcategory.CategoryPresenter;
import com.mastercard.bda.presentation.selectcity.SelectCityActivity;
import com.mastercard.bda.presentation.widget.DialogHelper;
import com.mastercard.bda.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ClickGuard;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vinh.trinh on 5/3/2017.
 */

public class HomeActivity extends NavDrawerActivity implements
        NavigationView.OnNavigationItemSelectedListener
        , HomeFragment.HomeEventsListener
        , InspireGalleryFragment.InspireGalleryEventListener
        , OnSuccessListener<Location> {

    public static final int REQUEST_CODE_ASK_CONCIERGE = 300;
    private static final String STATE_LAST_SELECTED = "last_selected";
    public DialogHelper dialogHelper;
    @Nullable @BindView(R.id.title)
    AppCompatTextView tvTitle;
    @Nullable @BindView(R.id.toolbar)
    Toolbar toolbar;
    boolean guarded = false;
    private int selectedMenuItemId = R.id.nav_home;
    private HomeViewPresenter viewPresenter;
    private LocationProvider locationProvider;

    private HomeViewPresenter viewPresenter() {
        return new HomeViewPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);
        ButterKnife.bind(this);
        navigationView.setNavigationItemSelectedListener(this);
        setupToolbar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_drawer);
        setTitle(R.string.title_home);
        dialogHelper = new DialogHelper(this);

        viewPresenter = viewPresenter();
        locationProvider = new LocationProvider(this);

        if (savedInstanceState == null) {
            viewPresenter.addFragment(HomeFragment.newInstance());
        } else {
            selectedMenuItemId = savedInstanceState.getInt(STATE_LAST_SELECTED);
            navigationView.setCheckedItem(selectedMenuItemId);
        }

        if(!CityData.citySelected()) {
            PreferencesStorageAspire prefStorage = new PreferencesStorageAspire(getApplicationContext());
            String selectedCity = prefStorage.selectedCity();
            CityData.setSelectedCity(selectedCity);
        }
        requestLocation();
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.HOME.getValue());
        }
        // Observe the back stack changed to decide the color of menu item
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                FragmentManager fm = getSupportFragmentManager();
                Fragment currentFrag = fm.findFragmentById(R.id.fragment_place_holder);
                if(currentFrag != null){
                    if(currentFrag instanceof HomeFragment){
                        selectedMenuItemId = R.id.nav_home;
                    }else if(currentFrag instanceof ExploreFragment){
                        selectedMenuItemId = R.id.nav_explore;
                    }
                }
                if(selectedMenuItemId != 0){
                    navigationView.setCheckedItem(selectedMenuItemId);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.setCheckedItem(selectedMenuItemId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        onPageColorChanged(ContextCompat.getColor(this, R.color.colorPrimaryDark));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        Drawable icConcierge = menu.findItem(R.id.action_ask_concierge).getIcon();
        ViewUtils.menuTintColors(this, icConcierge);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(!guarded) {
            ClickGuard.guard(ButterKnife.findById(toolbar, R.id.action_ask_concierge));
            guarded = true;
        }
        if (item.getItemId() == R.id.action_ask_concierge) {
            navigateToAskConcierge();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        //FragmentManager fm = getSupportFragmentManager();
        //set back toolbar color to default before switch between fragments.
        onPageColorChanged(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        switch (id) {
            case R.id.nav_home:
                setTitle(R.string.title_home);
//                if(fm.getBackStackEntryCount()>1)
//                    fm.popBackStack();
                viewPresenter.addFragment(HomeFragment.newInstance());
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_ask:
                navigateToAskConcierge();
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_explore:
                navigateToExplore();
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_profile:
                viewPresenter.navigateToMyProfile();
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_change_pass:
                viewPresenter.navigateToChangePassword();
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_preferences:
                viewPresenter.navigateToPreferences();
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_about:
                viewPresenter.navigateToMasterCardUtility(AppConstant.MASTERCARD_COPY_UTILITY.About);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_privacy:
                viewPresenter.navigateToMasterCardUtility(AppConstant.MASTERCARD_COPY_UTILITY.Privacy);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_term_of_use:
                viewPresenter.navigateToMasterCardUtility(AppConstant.MASTERCARD_COPY_UTILITY.TermsOfUse);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_sign_out:
                // Track "Sign out"
                App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.SIGN_OUT.getValue(),
                                        AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                                        AppConstant.GA_TRACKING_LABEL.SIGN_OUT.getValue());
                viewPresenter.signOut();
                break;
        }
        navigationView.setCheckedItem(selectedMenuItemId);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }
        FragmentManager fm = getSupportFragmentManager();
        int backStackEntryCount = fm.getBackStackEntryCount();
        if (backStackEntryCount > 1) {
            FragmentManager.BackStackEntry backEntry = fm.getBackStackEntryAt(backStackEntryCount - 1);
            if (backEntry.getName().equals(InspireGalleryFragment.class.getSimpleName())) {
                onPageColorChanged(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
//            fm.popBackStack();
            selectedMenuItemId = R.id.nav_home;
            setTitle(R.string.title_home);
            navigationView.setCheckedItem(selectedMenuItemId);
            if(!backEntry.getName().equalsIgnoreCase(HomeFragment.class.getSimpleName())){
                viewPresenter.addFragmentAfterClearAll(HomeFragment.newInstance());
            }else {
                finish();
            }
        } else {
            finish();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_LAST_SELECTED, selectedMenuItemId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setTitle(int title) {
        tvTitle.setText(title);
    }

    @Override
    public void setTitle(CharSequence title) {
        tvTitle.setText(title);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FragmentManager fm = getSupportFragmentManager();
        if (requestCode == REQUEST_CODE_ASK_CONCIERGE) {
            if (resultCode == ResultCode.RESULT_OK && data != null
                    && data.getBooleanExtra(IntentConstant.RESET_EXPLORE_PAGE, false)) {
                Fragment exploreFragment = fm.findFragmentByTag(ExploreFragment.class.getSimpleName());
                if (exploreFragment == null) {
                    navigateToExplore();
                } else {
                    fm.beginTransaction().show(exploreFragment).commit();
                    selectedMenuItemId = R.id.nav_explore;
                    navigationView.setCheckedItem(selectedMenuItemId);
                }
            }
        } else if(requestCode == RequestCode.SELECT_CITY) {
            if (resultCode == ResultCode.RESULT_OK) {
                navigateToExplore();
            }
        } else if(requestCode == RequestCode.PROFILE) {
            if (resultCode == ResultCode.RESULT_OK) viewPresenter.loadLocationCriteria();
        } else if(requestCode == RequestCode.USER_PREFERENCES) {
            if (resultCode == ResultCode.RESULT_OK) viewPresenter.loadCuisineCriteria();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(intent != null && intent.getBooleanExtra(IntentConstant.RESET_EXPLORE_PAGE, false)){
            if(!CityData.citySelected()) {
                Intent intent1 = new Intent(this, SelectCityActivity.class);
                startActivityForResult(intent1, RequestCode.SELECT_CITY);
                return;
            }
            CategoryPresenter.CATEGORY_ALL = true;
            setTitle(R.string.explore);
            viewPresenter.addSingleInstanceFragment(ExploreFragment.newInstance(viewPresenter.location, viewPresenter.cuisine));
            selectedMenuItemId = R.id.nav_explore;
            navigationView.setCheckedItem(selectedMenuItemId);
        }
    }

    @Override
    public void navigateToInspirationGallery() {
        setTitle(R.string.title_home);
        viewPresenter.addFragment(InspireGalleryFragment.newInstance());
    }

    @Override
    public void navigateToExplore() {
        if(!CityData.citySelected()) {
            Intent intent = new Intent(this, SelectCityActivity.class);
            startActivityForResult(intent, RequestCode.SELECT_CITY);
            return;
        }
        CategoryPresenter.CATEGORY_ALL = true;
        setTitle(R.string.explore);
        viewPresenter.addFragment(ExploreFragment.newInstance(viewPresenter.location, viewPresenter.cuisine));
        selectedMenuItemId = R.id.nav_explore;
        navigationView.setCheckedItem(selectedMenuItemId);
    }

    @Override
    public void navigateToAskConcierge() {
        viewPresenter.navigateToAskConcierge();
    }

    @Override
    public void onPageColorChanged(int color) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_place_holder);
        if(currentFragment != null && currentFragment instanceof InspireGalleryFragment){
            setStatusBarColor(color);
            setToolbarColor(color);
        }else{
            setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
    }

    @Override
    public void onSuccess(Location location) {
        viewPresenter.loadPreferences(location);
    }

    public void requestLocation() {
        locationProvider.getLocation();
    }

}
