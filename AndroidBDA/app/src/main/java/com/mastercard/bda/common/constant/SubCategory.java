package com.mastercard.bda.common.constant;

/**
 * Created by tung.phan on 6/1/2017.
 */

public interface SubCategory {

    String ACCOMMODATIONS = "ACCOMMODATIONS";
    String BARS = "BARES/BALADAS";
    String CULTURE = "CULTURAL";
    String DINING = "RESTAURANTES";
    String SHOPPING = "COMPRAS";
    String SPAS = "SPAS";

}
