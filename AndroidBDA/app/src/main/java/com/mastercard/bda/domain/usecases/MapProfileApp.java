package com.mastercard.bda.domain.usecases;

import com.api.aspire.data.entity.profile.UpdateProfileAspireRequest;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.usecases.MapProfileBase;
import com.mastercard.bda.common.constant.CityData;
import com.mastercard.bda.domain.mapper.profile.MapDataApi;
import com.mastercard.bda.domain.mapper.profile.MapDataProfile;

import io.reactivex.Single;

public class MapProfileApp implements MapProfileBase {

    public Single<ProfileAspire> mapResponse(PreferencesStorageAspire preferencesStorage,
                                             final ProfileAspire profileAspire,
                                             String secretKeyCurrent,
                                             String userEmailCurrent) {
        return Single.create(emitter -> {

            ProfileAspire profile = MapDataProfile.mapResponse(profileAspire, secretKeyCurrent, userEmailCurrent);

            //-- save select city local
            String nameCity = CityData.cityNameDefaultEmpty();
            preferencesStorage.editor().selectedCity(nameCity).build().save();

            emitter.onSuccess(profile);
        });
    }

    @Override
    public UpdateProfileAspireRequest mapUpdate(ProfileAspire profile, String passCodeLocal) {
        return MapDataProfile.mapUpdate(profile, passCodeLocal);
    }

    @Override
    public String mapSalutation(String profileSalutation) {
        return MapDataApi.mapSalutationRequest(profileSalutation);
    }

}
