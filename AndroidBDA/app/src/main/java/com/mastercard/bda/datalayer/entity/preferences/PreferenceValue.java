package com.mastercard.bda.datalayer.entity.preferences;

import com.google.gson.annotations.SerializedName;

public class PreferenceValue {
    @SerializedName("MYPREFERENCESID")
    private String myPreferencesID;
    @SerializedName("TYPE")
    private String type;
    @SerializedName("VALUE")
    private String value;
    @SerializedName("VALUE3")
    private String extraValue;
    @SerializedName("VALUE1")
    private String extraValue2;

    public String myPreferencesID() {
        return myPreferencesID;
    }

    public String type() {
        return type;
    }

    public String value() {
        return value;
    }

    public String extraValue() {
        return extraValue;
    }
    public String extraValue2(){return extraValue2;}
}