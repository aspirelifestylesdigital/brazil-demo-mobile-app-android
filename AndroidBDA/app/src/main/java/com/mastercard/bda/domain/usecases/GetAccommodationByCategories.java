package com.mastercard.bda.domain.usecases;

import android.text.TextUtils;

import com.mastercard.bda.App;
import com.mastercard.bda.R;
import com.mastercard.bda.common.constant.AppConstant;
import com.mastercard.bda.datalayer.entity.Tiles;
import com.mastercard.bda.domain.mapper.CCACategoriesMapper;
import com.mastercard.bda.domain.mapper.FilterMapper;
import com.mastercard.bda.domain.model.explore.ExploreRView;
import com.mastercard.bda.domain.model.explore.ExploreRViewItem;
import com.mastercard.bda.domain.repository.B2CRepository;
import com.mastercard.bda.presentation.explore.ExplorePresenter;
import com.mastercard.bda.presentation.explore.SearchConditions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;


/**
 * Created by vinh.trinh on 6/6/2017.
 */

public class GetAccommodationByCategories extends UseCase<List<ExploreRViewItem>, GetAccommodationByCategories.Params> {

    private B2CRepository b2CRepository;
    private List<Tiles> data;

    public GetAccommodationByCategories(B2CRepository b2CRepository) {
        this.b2CRepository = b2CRepository;
        data = new ArrayList<>();
    }

    @Override
    Observable<List<ExploreRViewItem>> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    public Single<List<ExploreRViewItem>> buildUseCaseSingle(Params params) {
        if(params.paging == ExplorePresenter.DEFAULT_PAGE) data.clear();
        return b2CRepository.getTilesList(params.categories)
                .map(tiles -> {
                    filterData(tiles, params.originalCategoryName);

                    filterGalleryItem(tiles);
                    FilterMapper.handleCCASubGeoRegion(tiles);

                    if(params.searchConditions != null) searchFilter(tiles, params.searchConditions);
                    int length = data.size();
                    data.addAll(tiles);
                    return viewData(length, tiles, params.originalCategoryName);
                });
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    private void filterData(List<Tiles> originalData, String originalCategoryName) {
        //CCACategoriesMapper categoriesMapper = new CCACategoriesMapper(CCACategoriesMapper.WHICH.ID);
        // do nothing as unnecessary
        //if(!categoriesMapper.shouldFilter(originalCategoryName)) return;
        // remove all items that are not belonging to 'originalCategory'
        /*List<Integer> _IDs = categoriesMapper.ids(originalCategoryName);
        List<Tiles> toBeRemoved = new ArrayList<>();
        for (Tiles tiles: originalData) {
            if(!_IDs.contains(tiles.ID())) toBeRemoved.add(tiles);
        }
        originalData.removeAll(toBeRemoved);*/
        removeId36(originalData);
        filterDining(originalData, originalCategoryName);
        filterTour(originalData, originalCategoryName);
        filterTravel(originalData, originalCategoryName);
        filterEntertainment(originalData, originalCategoryName);
        filterGolf(originalData, originalCategoryName);
        FilterMapper.filterTransportation(originalData, originalCategoryName);
        FilterMapper.filterSpecialtyTravel(originalData);
        //removeGolf(originalData);
    }
    private void removeId36(List<Tiles> originalData){
        Iterator<Tiles> iterator = originalData.iterator();
        while (iterator.hasNext()){
            Tiles tiles = iterator.next();
            if(tiles.ID() == 36){
                iterator.remove();
                break;
            }
        }
    }
    private void filterDining(List<Tiles> originalData, String originalCategoryName){
        if(originalCategoryName != null && originalCategoryName.equalsIgnoreCase("dining")){
            Iterator<Tiles> iterator = originalData.iterator();
            while (iterator.hasNext()){
                Tiles tiles = iterator.next();
                if(!("Dining".equalsIgnoreCase(tiles.category()) ||
                        "Culinary Experiences".equalsIgnoreCase(tiles.subCategory()))){
                    iterator.remove();
                }
            }
        }
    }
    private void filterTour(List<Tiles> originalData, String originalCategoryName){
        if(originalCategoryName != null && originalCategoryName.equalsIgnoreCase("tours")){
            Iterator<Tiles> iterator = originalData.iterator();
            while (iterator.hasNext()){
                Tiles tiles = iterator.next();
                if(!"Sightseeing".equalsIgnoreCase(tiles.subCategory())){
                    iterator.remove();
                }
            }
        }
    }
    private void filterTravel(List<Tiles> originalData, String originalCategoryName){
        if(originalCategoryName != null && originalCategoryName.equalsIgnoreCase("travel")){
            Iterator<Tiles> iterator = originalData.iterator();
            while (iterator.hasNext()){
                Tiles tiles = iterator.next();
                if(!("Private Jet Travel".equalsIgnoreCase(tiles.category()) ||
                        "Airport Services".equalsIgnoreCase(tiles.subCategory()) ||
                        "Travel Services".equalsIgnoreCase(tiles.subCategory()))){
                    iterator.remove();
                }
            }
        }
    }
    private void filterVipTravelService(List<Tiles> originalData, String originalCategoryName){
        if(originalCategoryName != null){
            AppConstant.EXPLORE_CATEGORY vipTravelService = null;
            if(originalCategoryName.equalsIgnoreCase("tours")){
                vipTravelService = AppConstant.EXPLORE_CATEGORY.TOUR_SUBCAT;
            }else if(originalCategoryName.equalsIgnoreCase("airport services")){
                vipTravelService = AppConstant.EXPLORE_CATEGORY.AIRPORT_SERVICES_SUBCAT;
            }else if(originalCategoryName.equalsIgnoreCase("travel services")){
                vipTravelService = AppConstant.EXPLORE_CATEGORY.TRAVEL_SERVICES_SUBCAT;
            }

            if(vipTravelService != null){
                Iterator<Tiles> tilesIterator = originalData.iterator();
                while (tilesIterator.hasNext()){
                    Tiles tiles = tilesIterator.next();
                    String subCateItem = tiles.subCategory();
                    if(!vipTravelService.getValue().equalsIgnoreCase(subCateItem)){
                        tilesIterator.remove();
                    }
                }
            }
        }
    }
    private void filterEntertainment(List<Tiles> originalData, String originalCategoryName){
        if(originalCategoryName != null && originalCategoryName.equalsIgnoreCase("entertainment")){
            Iterator<Tiles> iterator = originalData.iterator();
            while (iterator.hasNext()){
                Tiles tiles = iterator.next();
                if(!("Tickets".equalsIgnoreCase(tiles.category()) || "Entertainment Experiences".equalsIgnoreCase(tiles.subCategory()) ||
                        "Major Sports Events".equalsIgnoreCase(tiles.subCategory()))){
                    iterator.remove();
                }
            }
        }
    }

    private void filterGolf(List<Tiles> originalData, String originalCategoryName) {
        if(originalCategoryName != null && originalCategoryName.equalsIgnoreCase("sports")) {
            Iterator<Tiles> iterator = originalData.iterator();
            while (iterator.hasNext()){
                Tiles tiles = iterator.next();
                if(!("Golf".equalsIgnoreCase(tiles.category())) && !("Golf Experiences".equalsIgnoreCase(tiles.subCategory()))){
                    iterator.remove();
                }
            }
        }
    }

    private void removeGolf(List<Tiles> originalData){
        Iterator<Tiles> iterator = originalData.iterator();
        while (iterator.hasNext()){
            Tiles tiles = iterator.next();
            if("Specialty Travel".equalsIgnoreCase(tiles.category()) && "Golf Experiences".equalsIgnoreCase(tiles.subCategory())){
                iterator.remove();
            }
        }
    }
    //-- Fix #filter gallery
    /** remove description only have content "..." ;*/
    private void filterGalleryItem(List<Tiles> originalData){
        List<Tiles> toBeRemoved = new ArrayList<>();
        for (Tiles tiles: originalData) {
            if(tiles.shortDescription().equalsIgnoreCase("..."))
                toBeRemoved.add(tiles);
        }
        originalData.removeAll(toBeRemoved);
    }
/*
    private void filterGeographicRegion(List<Tiles> originalData) {
        //-- Fix #UDA-203
        //List<Integer> checkIDs = CityData.TitleContentIDs;
        List<Tiles> toBeRemoved = new ArrayList<>();
        String[] geographicRegions = CityData.geographicRegion().split("\\.");
        for (Tiles tiles: originalData) {
            //if(!checkIDs.contains(tiles.ID())) continue;
            if(TextUtils.isEmpty(tiles.subCategory()) || "Global Partners".equals(tiles.subCategory())) continue;
            if(!"Hotels".equalsIgnoreCase(tiles.category()) &&
                    !"Vacation Packages".equalsIgnoreCase(tiles.category()) &&
                    !"Cruises".equalsIgnoreCase(tiles.category())) continue;
            if(shouldIgnore(CityData.isCity(), geographicRegions, ccaSubCateLocal, cityNameSelected, tiles)) toBeRemoved.add(tiles);
        }
        originalData.removeAll(toBeRemoved);
    }
*/

    private void searchFilter(List<Tiles> data, SearchConditions searchConditions) {
        String term = searchConditions.term.toLowerCase();
        List<Tiles> toBeRemoved = new ArrayList<>();
        for (Tiles tiles : data) {
            if(tiles.title() != null && tiles.title().toLowerCase().contains(term)) continue;
            if(tiles.text() != null && tiles.text().toLowerCase().contains(term)) continue;
            if(tiles.shortDescription() != null && tiles.shortDescription().toLowerCase()
                    .contains(term)) continue;
            toBeRemoved.add(tiles);
        }
        data.removeAll(toBeRemoved);
    }

    private List<ExploreRViewItem> viewData(int startIndex, List<Tiles> dataList, String categoryName) {
        List<ExploreRViewItem> exploreRViewList = new ArrayList<>();
        int length = dataList.size();
        CCACategoriesMapper ccaCategoriesMapper = new CCACategoriesMapper(CCACategoriesMapper.WHICH.NAME);
        for (int i = 0; i < length; i++) {
            Tiles tiles = dataList.get(i);
            exploreRViewList.add(viewDatum(i + startIndex, tiles, ccaCategoriesMapper, categoryName));
        }
        return exploreRViewList;
    }

    private ExploreRViewItem viewDatum(int dataIndex, Tiles tiles, CCACategoriesMapper ccaCategoriesMapper, String categoryName) {
        ExploreRViewItem exploreRViewItem = new ExploreRViewItem(
                tiles.ID(),
                tiles.title().trim(),
                ccaCategoriesMapper.multipleCities(tiles.ID()) ? App.getInstance().getString(R.string.text_multiple_cities) : " ",
                tiles.image(),
                true,
                tiles.text(),
                dataIndex
        );
//        String categoryName = ccaCategoriesMapper.categoryName(tiles.ID());
//       eploreRViewItem.categoryName(categoryName == null ? "Hotels" : categoryName);
        if("all".equalsIgnoreCase(categoryName)){
            if("Golf".equalsIgnoreCase(tiles.category())){
                exploreRViewItem.categoryName(App.getInstance().getString(R.string.category_sports_logic_text));
            }else if("Specialty Travel".equalsIgnoreCase(tiles.category())){
                exploreRViewItem.categoryName(App.getInstance().getString(R.string.category_sports_logic_text));
            }else{
                exploreRViewItem.categoryName(tiles.category());
            }
        }else {
            exploreRViewItem.categoryName(tiles.category());
        }
        exploreRViewItem.subCategoryName(tiles.subCategory());
        return exploreRViewItem;
    }
/*

    private boolean shouldIgnore(boolean isUSCity, String[] geographicRegions, Tiles tiles) {
        String geographicRegion = tiles.geographicRegion();
        String subCategory = tiles.subCategory();
        if(geographicRegions.length== 0 || geographicRegions[0].equals("")) return true;
        if(!subCategory.contains(geographicRegions[0])) return true;
        if(!isUSCity && geographicRegions.length == 1) return false;
        for (String region : geographicRegions) {
            if(geographicRegion.contains(region)) return false;
        }
        return true;
    }
*/

    public ExploreRViewItem getItemView(int index) {
        Tiles tiles = data.get(index);
        ExploreRViewItem item = new ExploreRViewItem(
                tiles.ID(),
                tiles.title(),
                null,
                null,
                true,
                null,
                0
        );
        item.setItemType(ExploreRView.ItemType.NORMAL);
        return item;
    }

    public static final class Params {
        private String originalCategoryName;
        final String[] categories;
        final int paging;
        final SearchConditions searchConditions;

        public Params(String originalCategoryName, int paging, SearchConditions searchConditions, String... categories) {
            this.originalCategoryName = originalCategoryName;
            this.paging = paging;
            this.categories = categories;
            this.searchConditions = searchConditions;
        }
    }
}
