package com.mastercard.bda.presentation.selectcategory;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mastercard.bda.App;
import com.mastercard.bda.R;
import com.mastercard.bda.common.constant.AppConstant;
import com.mastercard.bda.common.constant.CityData;
import com.mastercard.bda.common.constant.IntentConstant;
import com.mastercard.bda.common.constant.RequestCode;
import com.mastercard.bda.common.constant.ResultCode;
import com.mastercard.bda.presentation.LocationPermissionHandler;
import com.mastercard.bda.presentation.base.CommonActivity;
import com.mastercard.bda.presentation.cityguidecategory.CityGuideCategoriesActivity;
import com.mastercard.bda.presentation.widget.DialogHelper;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 5/10/2017.
 */

public class SelectCategoryActivity extends CommonActivity implements
        CategoryAdapter.OnCategoryItemClickListener
        , Category.View {

    @BindView(R.id.selection_recycle_view)
    RecyclerView categoriesListView;
    private CategoryAdapter adapter;
    private DialogHelper dialogHelper;
    private CategoryPresenter presenter;
    LocationPermissionHandler locationPermissionHandler = new LocationPermissionHandler();

    private CategoryPresenter presenter() {
        return new CategoryPresenter(getApplicationContext());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_category);
        setTitle(R.string.category_title);
        setToolbarColor(R.color.colorPrimaryDark);
        presenter = presenter();
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        GridLayoutManager manager = new GridLayoutManager(this, 2);
        categoriesListView.setLayoutManager(manager);

        adapter = new CategoryAdapter(this);
        adapter.setListener(this);
        categoriesListView.setAdapter(adapter);
        categoriesListView.addItemDecoration(
                new SelectCategorySpacesItemDecoration(
                        getResources().getDimensionPixelSize(R.dimen.grid_item_margin)));
        presenter.loadProfile();

        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0 || position == 11) {
                    return 2;
                }
                return 1;
            }
        });

        categoriesListView.setPadding(getResources().getDimensionPixelSize(R.dimen.padding_medium),
                getResources().getDimensionPixelSize(R.dimen.padding_medium),
                getResources().getDimensionPixelSize(R.dimen.padding_medium),
                getResources().getDimensionPixelSize(R.dimen.padding_medium));

        if (savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.CATEGORY_LIST.getValue());
        }
    }

    @OnClick(R.id.btn_all_category)
    void allCategoryBtnClick(){
        returnResult("");
    }

    @Override
    public void onItemClick(int position, String category) {
        if (position == 10) {//click on City Guide
            //return the category id of the current city selected if that city support city guide
            int cityId = CityData.guideCode();
            //start sub category select
            Intent intent = new Intent(this, CityGuideCategoriesActivity.class);
            intent.putExtra(IntentConstant.CATEGORY_ID, cityId);
            startActivityForResult(intent, RequestCode.SELECT_SUB_CATEGORY);
        } else {
            if (position == 0) { // Dining
                if (presenter.isLocationProfileSettingOn()) {
                    if (!locationPermissionHandler.hasPermission(this)) {
                        locationPermissionHandler.requestPermission(this);
                    } else {
                        presenter.locationServiceCheck(getApplicationContext());
                    }
                } else {
                    returnResult(category);
                }
            } else {
                returnResult(category);
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        boolean granted = locationPermissionHandler.handlingPermissionResult(requestCode, grantResults);
        if (granted) {
            presenter.locationServiceCheck(getApplicationContext());
        } else {
            proceedWithDiningCategory();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCode.SELECT_SUB_CATEGORY) {
            if (resultCode == ResultCode.RESULT_OK && null != data) {
                data.putExtra(IntentConstant.SUPPER_CATEGORY
                        , getResources().getString(R.string.city_guide));
                setResult(ResultCode.RESULT_OK_WITH_ID, data);
                finish();
            }
        }
        if (requestCode == 11234) {
            returnResult(App.getInstance().getString(R.string.category_dining_text));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }

    @Override
    public void proceedWithDiningCategory() {
        returnResult(App.getInstance().getString(R.string.category_dining_text));
    }

    @Override
    public void askForLocationSetting() {
        dialogHelper.action(null, getApplicationContext().getString(R.string.dialogLocationContent),
                getApplicationContext().getString(R.string.textSetting), getApplicationContext().getString(R.string.text_cancel),
                (dialogInterface, i) -> openGPSSetting(),
                (dialogInterface, i) -> returnResult(App.getInstance().getString(R.string.category_dining_text)));
    }

    private void returnResult(String category) {
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.SELECTED_CATEGORY, category);
        setResult(ResultCode.RESULT_OK, intent);
        finish();
    }

    private void openGPSSetting() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, 11234);
    }
}
