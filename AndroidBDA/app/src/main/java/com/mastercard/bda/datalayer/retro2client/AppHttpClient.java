package com.mastercard.bda.datalayer.retro2client;

import com.mastercard.bda.BuildConfig;
import com.mastercard.bda.datalayer.restapi.B2CContentApi;
import com.mastercard.bda.datalayer.restapi.B2CInstantApi;
import com.mastercard.bda.datalayer.restapi.B2CUtilityApi;
import com.mastercard.bda.datalayer.restapi.CCAApi;
import com.mastercard.bda.datalayer.restapi.GeoCoderApi;

import java.util.concurrent.TimeUnit;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tung.phan on 5/11/2017.
 * Singleton AppHttpClient
 */

public class AppHttpClient extends Retro2Client {

    private B2CInstantApi b2cInstantApi;
    private B2CContentApi b2CContentApi;
    private B2CUtilityApi b2CUtilityApi;
    private CCAApi b2CTilesApi;
    private GeoCoderApi geoCoderApi;

    private static class AppHttpClientHelper {
        private static final AppHttpClient INSTANCE = new AppHttpClient();
    }

    public static AppHttpClient getInstance() {
        return AppHttpClientHelper.INSTANCE;
    }

    public B2CInstantApi getB2CInstantApi() {
        return b2cInstantApi;
    }

    public B2CContentApi getB2CContentApi() {
        return b2CContentApi;
    }

    public B2CUtilityApi getB2CUtilityApi() {
        return b2CUtilityApi;
    }

    public CCAApi getB2CTilesApi() {
        return b2CTilesApi;
    }

    public GeoCoderApi geoCoderApi() {
        return geoCoderApi;
    }

    private AppHttpClient() {
        final Retrofit b2cRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_B2C_ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClientWithoutCache())
                .build();
        Retrofit b2cCCARetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_B2C_ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClient(0, TimeUnit.MINUTES))
                .build();

        Retrofit geoCoderApiRetrofit = new Retrofit.Builder()
                .baseUrl("http://maps.google.com/maps/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClient(7, TimeUnit.DAYS))
                .build();

        b2cInstantApi = b2cRetrofit.create(B2CInstantApi.class);
        b2CContentApi = b2cRetrofit.create(B2CContentApi.class);
        b2CUtilityApi = b2cRetrofit.create(B2CUtilityApi.class);
        b2CTilesApi = b2cCCARetrofit.create(CCAApi.class);
        geoCoderApi = geoCoderApiRetrofit.create(GeoCoderApi.class);
    }

}
