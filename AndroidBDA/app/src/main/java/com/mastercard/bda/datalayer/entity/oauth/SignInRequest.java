package com.mastercard.bda.datalayer.entity.oauth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 7/18/2017.
 */

public class SignInRequest {
    @Expose
    @SerializedName("ConsumerKey")
    private final String consumerKey;
    @Expose
    @SerializedName("Functionality")
    private final String functionality;
    @Expose
    @SerializedName("MemberDeviceId")
    private final String memberDeviceId;
    @Expose
    @SerializedName("Email2")
    private final String email;
    @Expose
    @SerializedName("Password")
    private final String secretKey;

    public SignInRequest(String memberDeviceId, String email, String password) {
        this.consumerKey = "DMA";
        this.functionality = "Login";
        this.memberDeviceId = "DeviceID10";
        this.email = email;
        this.secretKey = password;
    }
}
