package com.mastercard.bda.domain.mapper;


import android.text.TextUtils;

import com.mastercard.bda.common.constant.CityCountry;
import com.mastercard.bda.common.constant.CityData;
import com.mastercard.bda.datalayer.entity.SearchContent;
import com.mastercard.bda.datalayer.entity.Tiles;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FilterMapper {


    /** filter ccaSubCategory and geographic region */
    public static void handleCCASubGeoRegion(List<Tiles> originalData) {
        List<Tiles> toBeRemoved = new ArrayList<>();
        boolean isCity = CityData.isCity();
        List<CityCountry> cityCountriesLocal = CityData.cityCountryList();
        for (Tiles tiles: originalData) {
            boolean isNeedRemove = isNeedRemoveItem(isCity, cityCountriesLocal, tiles.category(), tiles.subCategory(), tiles.geographicRegion());
            if (isNeedRemove) {
                toBeRemoved.add(tiles);
            }//do nothing, show this item => continue
        }
        originalData.removeAll(toBeRemoved);
    }

    /** Page Search filter ccaSubCategory and geographic region */
    public static void handleCCASubGeoRegionSearch(List<SearchContent> originalData) {
        List<SearchContent> toBeRemoved = new ArrayList<>();
        boolean isCity = CityData.isCity();
        List<CityCountry> cityCountriesLocal = CityData.cityCountryList();
        for (SearchContent tiles: originalData) {
            boolean isNeedRemove = isNeedRemoveItem(isCity, cityCountriesLocal, tiles.category(),tiles.subCategory(), tiles.geographicRegion());
            if (isNeedRemove) {
                toBeRemoved.add(tiles);
            }//do nothing, show this item => continue
        }
        originalData.removeAll(toBeRemoved);
    }

    private static final String VALUE_CATEGORY_GLOBAL_PARTNERS = "Global Partners";
    private static final String VALUE_CATEGORY_HOTELS = "Hotels";
    private static final String VALUE_CATEGORY_VACATION_PACKAGES = "Vacation Packages";
    private static final String VALUE_CATEGORY_CRUISES = "Cruises";
    private static final String VALUE_CATEGORY_GLOBAL_OFFERS = "Global Offers";

    /**
     * check with subCategory and geographic
     * @return true will remove item, otherwise false will hold item
     * */
    private static boolean isNeedRemoveItem(boolean isCity, List<CityCountry> cityCountriesLocal,
                                            String categoryTypeRemote, String subCatRemote, String geoRegionRemote) {

        boolean isRemoveItem = true;
        geoRegionRemote = geoRegionRemote.trim();

        if (VALUE_CATEGORY_HOTELS.equalsIgnoreCase(categoryTypeRemote) ||
                VALUE_CATEGORY_VACATION_PACKAGES.equalsIgnoreCase(categoryTypeRemote) ||
                VALUE_CATEGORY_CRUISES.equalsIgnoreCase(categoryTypeRemote)) {
            //<editor-fold desc="case Category Hotels, Cruises, Vacation Packages">
            //check in CityCountry
            if (TextUtils.isEmpty(subCatRemote) || VALUE_CATEGORY_GLOBAL_PARTNERS.equals(subCatRemote)) {
                isRemoveItem = false;
            } else{
                boolean checkInternalMapping;
                for (CityCountry cityCountry : cityCountriesLocal) {
                    checkInternalMapping = true;
                    String subCateLocal = cityCountry.getSubCategory();
                    String geoLocal = cityCountry.getGeographic();
                    if (subCatRemote.equalsIgnoreCase(subCateLocal) && geoRegionRemote.equalsIgnoreCase(geoLocal)) {
                        //-- same ccaSub & geographic
                        checkInternalMapping = false;
                    } else if (subCateLocal.equalsIgnoreCase(CityData.VALUE_SUB_CCA_CATEGORY_ANY) && geoRegionRemote.equalsIgnoreCase(geoLocal)) {
                        //-- non check subCate with Any & only check geographic
                        checkInternalMapping = false;
                    } else if (!isCity && subCatRemote.contains(subCateLocal) && geoLocal.equalsIgnoreCase(CityData.VALUE_GEOGRAPHIC_ALL_EXCEPT_CITY)) {
                        //-- is Region & same ccaSub & geographic get all (except geo of city)
                        String mappingCity = cityCountry.getMappingCity();
                        String exceptCity = cityCountry.getExceptCity();

                        //-- default show all, only remove except city
                        checkInternalMapping = handleMappingExceptCity(geoRegionRemote, mappingCity, exceptCity);

                    } else if (!isCity && subCatRemote.equalsIgnoreCase(subCateLocal) && geoLocal.equalsIgnoreCase(CityData.VALUE_GEOGRAPHIC_REGION_ALL)) {
                        //-- is Region & same ccaSub & geographic get all
                        checkInternalMapping = false;

                    } else if (subCatRemote.equalsIgnoreCase(subCateLocal) && geoLocal.contains(".")) {
                        //- is City, arr geographic local check with geo remote
                        for (String region : geoLocal.split("\\.")) {
                            if (isSameCountry(geoRegionRemote, region)) {
                                checkInternalMapping = false;
                                break; //jump out for region
                            }
                        }
                    }

                    //after check in cityCountry return final result
                    if (!checkInternalMapping) {
                        isRemoveItem = false;
                    }
                }//-- close for
            }
            //-- finish check return result

            //</editor-fold>
        } else if (VALUE_CATEGORY_GLOBAL_OFFERS.equalsIgnoreCase(categoryTypeRemote)
                && !TextUtils.isEmpty(subCatRemote)
                && !TextUtils.isEmpty(geoRegionRemote)) {
            isRemoveItem = false;

        } else {
            //-- will don't filter subCateRemote, only check filter geoRemote, subCateLocal, geoLocal
            //- don't check case geoLocal get All
            //<editor-fold desc="case Category: Dining, Entertainment, Shopping, Flower, Transportation, Travel, Tour, Sport">
            if (TextUtils.isEmpty(geoRegionRemote)) {
                //-- subCate: Any and geoRemote: bank
                isRemoveItem = false;
            } else {
                //check in CityCountry
                boolean checkInternalMapping;
                for (CityCountry cityCountry : cityCountriesLocal) {
                    checkInternalMapping = true;
                    String subCateLocal = cityCountry.getSubCategory();
                    String geoLocal = cityCountry.getGeographic();
                    if (geoRegionRemote.equalsIgnoreCase(geoLocal)) {
                        //-- same ccaSub & geographic
                        checkInternalMapping = false;
                    } else if (subCateLocal.equalsIgnoreCase(CityData.VALUE_SUB_CCA_CATEGORY_ANY) && geoRegionRemote.equalsIgnoreCase(geoLocal)) {
                        //-- non check subCate with Any & only check geographic
                        checkInternalMapping = false;
                    } else if (!isCity && geoLocal.equalsIgnoreCase(CityData.VALUE_GEOGRAPHIC_ALL_EXCEPT_CITY)) {
                        //-- is Region & geographic get all (except geo of city)
                        String mappingCity = cityCountry.getMappingCity();

                        //-- default remove all, only show mapping city
                        if(!TextUtils.isEmpty(mappingCity)){
                            for (String region : mappingCity.split("\\.")) {
                                if (isSameCountry(geoRegionRemote, region)) {
                                    checkInternalMapping = false;
                                    break;
                                }
                            }
                        }

                    } else if (geoLocal.contains(".")) {
                        //- is City, arr geographic local check with geo remote
                        for (String region : geoLocal.split("\\.")) {
                            if (isSameCountry(geoRegionRemote, region)) {
                                checkInternalMapping = false;
                                break; //jump out for region
                            }
                        }
                    }

                    //after check in cityCountry return final result
                    if (!checkInternalMapping) {
                        isRemoveItem = false;
                    }
                }//-- close for

                //-- finish check return result
            }
            //</editor-fold>
        }
        return isRemoveItem;
    }

    //-- filter remove data don't show on view
    /** @return true: remove item, false show item on view*/
    private static boolean handleMappingExceptCity(String geoRegionRemote, String mappingCity, String exceptCity){
        //default will show data
        boolean needShowData = false;
        //-- check mapping
        if(!TextUtils.isEmpty(mappingCity)){
            for (String region : mappingCity.split("\\.")) {
                if (isSameCountry(geoRegionRemote, region)) {
                    needShowData = false;
                    break;
                }
            }
        }
        //-- check expect city
        if(!TextUtils.isEmpty(exceptCity)){
            for(String expectVal : exceptCity.split("\\.")){
                if(isSameCountry(geoRegionRemote, expectVal)){
                    needShowData = true;
                    break;
                }
            }
        }

        return needShowData;
    }

    private static boolean isSameCountry(String geoRegionRemote, String regionLocal){
        return geoRegionRemote.contains(regionLocal) || regionLocal.contains(geoRegionRemote);
    }

    //<editor-fold desc="Transportation">
    public static void filterTransportation(List<Tiles> originalData, String originalCategoryName){
        if(originalCategoryName != null && originalCategoryName.equalsIgnoreCase("transportation")){
            Iterator<Tiles> iterator = originalData.iterator();
            while (iterator.hasNext()){
                Tiles tiles = iterator.next();
                if(!("Car Rental".equalsIgnoreCase(tiles.subCategory()) ||
                        "Limos and Private Car Service".equalsIgnoreCase(tiles.subCategory()))){
                    iterator.remove();
                }
            }
        }
    }

    public static void filterTransportationSearch(List<SearchContent> originalData, String originalCategoryName){
        if(originalCategoryName != null && originalCategoryName.equalsIgnoreCase("transportation")){
            Iterator<SearchContent> iterator = originalData.iterator();
            while (iterator.hasNext()){
                SearchContent tiles = iterator.next();
                if(!("Car Rental".equalsIgnoreCase(tiles.subCategory()) ||
                        "Limos and Private Car Service".equalsIgnoreCase(tiles.subCategory()))){
                    iterator.remove();
                }
            }
        }
    }
    //</editor-fold>

    //<editor-fold desc="SpecialtyTravel">
    /* Special Travel case with subcategory === null remove*/
    public static void filterSpecialtyTravel(List<Tiles> originalData) {
        Iterator<Tiles> iterator = originalData.iterator();
        while (iterator.hasNext()) {
            Tiles tiles = iterator.next();
            if ("Specialty Travel".equalsIgnoreCase(tiles.category()) && tiles.subCategory() == null) {
                iterator.remove();
            }
        }
    }

    public static void filterSpecialtyTravelSearch(List<SearchContent> originalData) {
        Iterator<SearchContent> iterator = originalData.iterator();
        while (iterator.hasNext()) {
            SearchContent tiles = iterator.next();
            if ("Specialty Travel".equalsIgnoreCase(tiles.category()) && tiles.subCategory() == null) {
                iterator.remove();
            }
        }
    }
    //</editor-fold>

}
