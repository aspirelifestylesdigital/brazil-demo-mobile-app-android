package com.mastercard.bda.presentation.checkout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.mastercard.bda.App;
import com.mastercard.bda.R;
import com.mastercard.bda.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.mastercard.bda.common.logic.Validator;
import com.mastercard.bda.presentation.base.BaseActivity;
import com.mastercard.bda.presentation.changepass.ChangePasswordActivity;
import com.mastercard.bda.presentation.home.HomeActivity;
import com.mastercard.bda.presentation.profile.ForgotPasswordActivity;
import com.mastercard.bda.presentation.profile.SignUpActivity;
import com.mastercard.bda.presentation.widget.DialogHelper;
import com.mastercard.bda.presentation.widget.PasswordInputFilter;
import com.mastercard.bda.presentation.widget.ViewKeyboardListener;
import com.mastercard.bda.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ButtonTextSpacing;
import com.support.mylibrary.widget.ErrorIndicatorEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends BaseActivity implements SignIn.View {

    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.edt_password)
    ErrorIndicatorEditText edtPassword;
    @BindView(R.id.btn_sign_in)
    ButtonTextSpacing btnSignIn;
    @BindView(R.id.img_background)
    ImageView imgBackground;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.scrollView)
    ScrollView scroll;
    @BindView(R.id.holder)
    FrameLayout llRootView;
    @BindView(R.id.llParent)
    LinearLayout llParent;
    DialogHelper dialogHelper;
    SignInPresenter presenter;
    Validator validator;

    private SignInPresenter buildPresenter() {
        return new SignInPresenter(getApplicationContext());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);
        final Handler handler = new Handler();
        dialogHelper = new DialogHelper(this);
        presenter = buildPresenter();
        presenter.attach(this);
        validator = new Validator();
        resizeBackgroundImage();
        keyboardInteractListener();

        btnSignIn.setEnabled(false);
        btnSignIn.post(new Runnable() {
            @Override
            public void run() {
                btnSignIn.setClickable(false);
            }
        });
        edtEmail.setOnFocusChangeListener((view1, b) -> {
            if (!b) {
                if (edtEmail.getText().toString().trim().length() == 0) {
                    edtEmail.setText("");
                    btnSignIn.setEnabled(false);
                }
                ViewUtils.hideSoftKey(edtEmail);
            } else {
                new Thread(() -> {
                    try {
                        Thread.sleep(350);
                    } catch (InterruptedException e) {
                    }
                    handler.post(() -> scroll.scrollTo(0, scroll.getBottom()));
                }).start();
            }
            edtEmail.setCursorVisible(b);
        });

        edtPassword.setOnFocusChangeListener((view1, b) -> {
            if (!b) {
                ViewUtils.hideSoftKey(edtPassword);
            } else {
                new Thread(() -> {
                    try {
                        Thread.sleep(350);
                    } catch (InterruptedException e) {
                    }
                    handler.post(() -> scroll.scrollTo(0, scroll.getBottom()));
                }).start();
            }
            edtPassword.setCursorVisible(b);
        });

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (edtEmail.getText().toString().trim().length() == 0) {
                    btnSignIn.setEnabled(false);
                } else {
                    boolean isEnabled = editable.toString().length() > 0
                            && edtPassword.getText().toString().length() > 0;

                    btnSignIn.setEnabled(isEnabled);
                    btnSignIn.setClickable(isEnabled);
                }
            }
        });

        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                boolean isEnabled = editable.toString().length() > 0
                        && edtEmail.getText().toString().length() > 0;
                btnSignIn.setEnabled(isEnabled);
                btnSignIn.setClickable(isEnabled);
            }
        });
        edtPassword.setCustomSelectionActionModeCallback(new android.view.ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(android.view.ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(android.view.ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(android.view.ActionMode actionMode, MenuItem menuItem) {
                return false;
            }

            @Override
            public void onDestroyActionMode(android.view.ActionMode actionMode) {
            }
        });
        edtPassword.setFilters(new InputFilter[]{new PasswordInputFilter(), new InputFilter.LengthFilter(getResources().getInteger(R.integer.password_max_length_characters))});
        /*edtEmail.setText("viet17@gmail.com");
        edtPassword.setText("123456");*/
        if (savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SIGN_IN.getValue());
        }
    }

    /**
     * handle click on back hide keyboard close cursor
     */
    private void keyboardInteractListener() {
        ViewKeyboardListener.KeyboardEvent event = new ViewKeyboardListener.KeyboardEvent() {
            @Override
            public void showKeyboard() {
                //dummy do nothing
            }

            @Override
            public void hideKeyboard() {
                //dummy do nothing
            }

            @Override
            public View getCurrentFocus() {
                return SignInActivity.this.getCurrentFocus();
            }
        };
        //-- add listen keyboard
        ViewKeyboardListener keyboardListener = new ViewKeyboardListener(llRootView, event);
        keyboardListener.setViewFocusChangeListener(llParent);
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @OnClick(R.id.btn_sign_in)
    public void signInClick(View view) {
        ViewUtils.hideSoftKey(view);
        String email = edtEmail.getText().toString();
        String password = edtPassword.getText().toString();

        //-- clean all error highlight
        edtEmail.setError(null);
        edtPassword.setError(null);

        final SignInValidate validate = validateFields(email, password);
        if (validate.isNoHaveErrorInput()) {
            presenter.doSignIn(email, password);
        } else {
            dialogHelper.profileDialog(validate.errorMgs, dialog -> {
                final ErrorIndicatorEditText edtError = validate.errorValidate;
                if (edtError != null) {
                    edtError.postDelayed(() -> {
                        edtError.requestFocus();
                        edtError.setError("");
                        edtError.setCursorVisible(true);
                        edtError.setSelection(edtError.getText().toString().length());

                        ViewUtils.showSoftKey(edtError);
                    }, 100);
                }
            });
        }
    }

    @OnClick(R.id.tv_sign_up)
    public void toSignUpView() {
        startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
        finish();
    }

    @OnClick(R.id.tv_forgot_pw)
    public void forgotPasswordClick() {
        clearForm();
        edtEmail.setError(null);
        edtPassword.setError(null);
        startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
//        finish();
    }

    @OnClick({R.id.holder, R.id.llParent})
    public void onClickOutsideView(View view) {
        hideAllEdtCursor();
    }

    private void hideAllEdtCursor() {
        edtEmail.setCursorVisible(false);
        edtPassword.setCursorVisible(false);
    }

    // presenter
    @Override
    public void proceedToHome() {
        boolean hasForgotPwd = new PreferencesStorageAspire(getApplicationContext()).hasForgotPwd();
        Intent intent;
        if (hasForgotPwd) {
            intent = new Intent(this, ChangePasswordActivity.class);
            intent.putExtra(Intent.EXTRA_REFERRER, "a");
        } else {
            intent = new Intent(this, HomeActivity.class);
        }
        startActivity(intent);
        finish();

        // Track GA with "Sign in" event
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                AppConstant.GA_TRACKING_LABEL.SIGN_IN.getValue());
    }

    @Override
    public void onCheckPassCodeFailure() {
//        dialogHelper.alert(App.getInstance().getString(R.string.dialogTitleError), getString(R.string.dialogErrorNonePassCode), v ->{
//            Intent intent = new Intent(this, NewPassCodeActivity.class);
//            startActivity(intent);
//            finish();
//        });


        dialogHelper.action(App.getInstance().getString(R.string.dialogTitleError), getString(R.string.dialogErrorNonePassCode), getString(R.string.text_ok), getString(R.string.text_cancel),
                (dialogInterface, i) -> toNewPassCodeScreen());
    }

    private void toNewPassCodeScreen() {
        Intent intent = new Intent(this, NewPassCodeActivity.class);
        startActivity(intent);
        finish();
    }

    private void toSignInScreen() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();

    }

    // presenter
    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        edtEmail.setError("");
        edtPassword.setError("");
        if (errCode == ErrCode.API_ERROR) dialogHelper.alert(getString(R.string.dialogTitleError), extraMsg);
        else dialogHelper.showGeneralError();
    }

    // presenter
    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    // presenter
    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    //<editor-fold desc="Validate edt input">
    private SignInValidate validateFields(String email, String password) {
        ErrorIndicatorEditText valid = null;
        StringBuilder stringBuilder = new StringBuilder();

        if (TextUtils.isEmpty(email.trim()) || TextUtils.isEmpty(password.trim())) {
            if (TextUtils.isEmpty(email)) edtEmail.setError("");
            if (TextUtils.isEmpty(password)) edtPassword.setError("");
            else edtPassword.setError(null);
            stringBuilder.append(getString(R.string.input_err_required));
            valid = edtEmail;
        }

        if (!TextUtils.isEmpty(email)) {
            if (!validator.email(email)) {
                edtEmail.setError("");
                if (stringBuilder.length() > 0) stringBuilder.append("\n");
                stringBuilder.append(getString(R.string.input_err_invalid_email));
                valid = edtEmail;
            } else {
                edtEmail.setError(null);
            }
        }

        if (TextUtils.isEmpty(password) || password.length() < 5) {
            edtPassword.setError("");
            if (stringBuilder.length() > 0) stringBuilder.append("\n");
            stringBuilder.append(getString(R.string.input_err_old_pwd_failed));
            valid = edtPassword;
        } else {
            edtPassword.setError(null);
        }

        String errMessage = stringBuilder.toString();
        return new SignInValidate(valid, errMessage);
    }

    private class SignInValidate {
        ErrorIndicatorEditText errorValidate = null;
        String errorMgs = "";

        SignInValidate(ErrorIndicatorEditText errorValidate, String errorMgs) {
            this.errorValidate = errorValidate;
            this.errorMgs = errorMgs;
        }

        boolean isNoHaveErrorInput() {
            return errorValidate == null;
        }
    }
    //</editor-fold>

    private void resizeBackgroundImage() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        imgBackground.getLayoutParams().height = height;
        imgBackground.getLayoutParams().width = width;
        imgBackground.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    private void clearForm() {
        edtEmail.setText("");
        edtPassword.setText("");
    }
}
