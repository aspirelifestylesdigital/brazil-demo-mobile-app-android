package com.mastercard.bda.datalayer.entity.preferences;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 7/27/2017.
 */

public class PreferenceDetail {

    public enum TYPE {CUISINE, HOTEL, TRANSPORTATION}

    @SerializedName("Type")
    private final String type;
    @SerializedName("CuisinePreferences")
    private final String cuisine;
    @SerializedName("Preferredstarrating")
    private final String starRating;
    @SerializedName("PreferredRentalVehicle")
    private final String vehicle;
    @SerializedName("Delete")
    private final Boolean delete;
    @SerializedName("MyPreferencesId")
    private final String preferenceID;
    @SerializedName("SmokingPreference")
    private final String locationSetting;
    @SerializedName("RoomPreference")
    private String passCode;
    @SerializedName("BedPreference")
    private final String nameApp;

    public PreferenceDetail(TYPE type, String value, String preferenceID) {
        this(type, value, preferenceID, null);
    }

    public PreferenceDetail(TYPE type, String value, String preferenceID, String extraValue) {
        switch (type) {
            case CUISINE:
                this.type = "Dining";
                cuisine = value;
                starRating = null;
                vehicle = null;
                nameApp = null;
                break;
            case HOTEL:
                this.type = "Hotel";
                cuisine = null;
                starRating = value;
                vehicle = null;
                nameApp = "BDA";
                break;
            case TRANSPORTATION:
                this.type = "Car Rental";
                cuisine = null;
                starRating = null;
                vehicle = value;
                nameApp = null;
                break;
            default:
                this.type = null;
                cuisine = null;
                starRating = null;
                vehicle = null;
                nameApp = null;
                break;
        }
        delete = false;
        this.preferenceID = preferenceID;
        this.locationSetting = extraValue;
    }
    public PreferenceDetail setPassCode(String passCode){
        this.passCode = passCode;
        return this;
       }
}
