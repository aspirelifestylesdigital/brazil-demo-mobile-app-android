package com.mastercard.bda.presentation.profile;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.mastercard.bda.App;
import com.mastercard.bda.R;
import com.mastercard.bda.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.mastercard.bda.common.logic.Validator;
import com.mastercard.bda.presentation.base.CommonActivity;
import com.mastercard.bda.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.ErrorIndicatorEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 7/24/2017.
 */

public class ForgotPasswordActivity extends CommonActivity implements ForgotPassword.View {

    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.scrollView)
    ScrollView scroll;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.img_background)
    ImageView imgBackground;
    @BindView(R.id.holder)
    FrameLayout holderForgotPaswdView;
    private ForgotPasswordPresenter presenter;
    private DialogHelper dialogHelper;
    private Validator validator;
    private boolean isKeyboardShow;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        btnSubmit.setEnabled(false);
        btnSubmit.post(new Runnable() {
            @Override
            public void run() {
                btnSubmit.setClickable(false);
            }
        });
        final Handler handler = new Handler();
        presenter = new ForgotPasswordPresenter();
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        validator = new Validator();
        resizeBackgroundImage();
//        initTextInteractListener();
        toolbar.setClickable(true);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(edtEmail);
            }
        });
        edtEmail.setOnFocusChangeListener((view1, b) -> {
            if (!b) {
                hideKeyboard(view1);
            } /*else {
                new Thread(() -> {
                    try {
                        Thread.sleep(350);
                    } catch (InterruptedException e) {}
                    handler.post(() -> scroll.scrollTo(0, scroll.getBottom()));
                }).start();
            }*/
        });

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                boolean isEnabled = editable.toString().length() > 0;
                btnSubmit.setEnabled(isEnabled);
                btnSubmit.setClickable(isEnabled);
            }
        });
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.FORGOT_PASSWORD.getValue());
        }
    }

    private void resizeBackgroundImage() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        imgBackground.getLayoutParams().height = height;
        imgBackground.getLayoutParams().width = width;
        imgBackground.setScaleType(ImageView.ScaleType.FIT_XY);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            hideKeyboard(edtEmail);
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_submit)
    void onSubmit(View view) {
        hideKeyboard(edtEmail);
        String email = edtEmail.getText().toString();
        if (!validator.email(email)) {
            dialogHelper.profileDialog(getString(R.string.input_err_invalid_email), dialog -> {
                if(view != null) {
                    view.postDelayed(() -> invokeSoftKey(edtEmail), 100);

                }
            });
        } else {
            showProgressDialog();
            presenter.retrievePassword(email);
        }
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void showErrorMessage(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        if(errCode == ErrCode.API_ERROR) {
            dialogHelper.alert(getResources().getString(R.string.input_err_fields),
                    getResources().getString(R.string.input_err_invalid_email));
        } else {
            dialogHelper.showGeneralError();
        }
    }

    @Override
    public void showSuccessMessage() {
        dialogHelper.alert(null, getString(R.string.retrieve_password_message), dialog -> onBackPressed());
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    void invokeSoftKey(View view) {
        if(edtEmail != null) {
            edtEmail.requestFocus();
            edtEmail.setError("");
            edtEmail.setCursorVisible(true);
            edtEmail.setSelection(edtEmail.getText().toString().length());
        }
        InputMethodManager inputMethodManager =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(
                view.getWindowToken(),
                InputMethodManager.SHOW_IMPLICIT, 0);
    }

    @Override
    public void onViewPositionChanged(float fractionAnchor, float fractionScreen) {
        if(fractionScreen == 1.0) hideKeyboard(edtEmail);
        super.onViewPositionChanged(fractionAnchor, fractionScreen);
    }
}
