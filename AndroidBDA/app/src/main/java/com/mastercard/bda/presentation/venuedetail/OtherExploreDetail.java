package com.mastercard.bda.presentation.venuedetail;

import com.mastercard.bda.domain.model.explore.OtherExploreDetailItem;
import com.mastercard.bda.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public interface OtherExploreDetail {

    interface View {
        void onGetContentFullFinished(OtherExploreDetailItem exploreRViewItem);
        void onUpdateFailed();
        void noInternetMessage();
    }

    interface Presenter extends BasePresenter<View> {
        void getContent(Integer contentId);
    }

}
