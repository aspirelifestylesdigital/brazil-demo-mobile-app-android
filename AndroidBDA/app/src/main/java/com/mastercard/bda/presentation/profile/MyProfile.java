package com.mastercard.bda.presentation.profile;

import com.api.aspire.common.constant.ErrCode;
import com.mastercard.bda.domain.model.Profile;
import com.mastercard.bda.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 5/11/2017.
 */

public interface MyProfile {

    interface View {
        void showProfile(Profile profile, boolean fromRemote);
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
        void profileUpdated();
    }

    interface Presenter extends BasePresenter<View> {
        void getProfile();
        void updateProfile(Profile profile);
        void abort();
    }
}
