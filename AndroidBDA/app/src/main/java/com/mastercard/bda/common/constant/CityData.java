package com.mastercard.bda.common.constant;

import android.text.TextUtils;
import android.util.SparseArray;

import com.mastercard.bda.App;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vinh.trinh on 6/2/2017.
 */

public final class CityData {
    /* warring change array cities */
    private static LinkedHashMap<String, City> cities;
    static final SparseArray<CityGuide> cityGuide;
    private static String SELECTED_CITY = "";
    public static final String DEFAULT_CITY = "All";
    public static final List<String> regions = Arrays.asList("Africa - Emirados Arabes", "América Latina", "Ásia",
            "Canadá", "Caribe", "EUA","Europa");

    //<editor-fold desc="Value config cca SubCategory and geographic region">
    public static final String VALUE_SUB_CCA_CATEGORY_ANY = "Any";
    public static final String VALUE_GEOGRAPHIC_REGION_ALL = "All";
    public static final String VALUE_GEOGRAPHIC_ALL_EXCEPT_CITY = "All_Except_City";
    //</editor-fold>

    //-- Fix #UDA-203
    // Update Data add ,1086,1370,1371,1372,1373,1374,1375;
    /** case: id in array, app skip process that object for get Category or SearchResult  */
    public static final List<Integer> cityGuideCodeList = Arrays.asList(
            7177,7178,7179,7180,7181,
            7033,7034,7035,7036,7037,
            7038,7039,7040,7041,7042,
            6990,6991,6992,6993,6994,
            7182,7183,7184,7185,7186,
            7043,7044,7045,7046,7047,
            7187,7188,7189,7190,7191,
            7192,7193,7194,7195,7196,
            7197,7198,7199,7200,7201,
            7058,7059,7060,7061,7062,
            7063,7064,7065,7066,7067,
            7207,7208,7209,7210,7211,
            7068,7069,7070,7071,7072,
            7212,7213,7214,7215,7216
    );
    static {
        cityGuide = new SparseArray<>();

        //<editor-fold desc="Config filter ccaSubCategory and Geographic">

        List<CityCountry> cityBariloche = new ArrayList<>(Collections.singletonList(
                new CityCountry("Latin America (South/Central America)", "Río Negro")
        ));

        List<CityCountry> cityBuenos = new ArrayList<>(Collections.singletonList(
                new CityCountry("Latin America (South/Central America)", "Buenos Aires.Ciudad Autónoma de Buenos Aires")
        ));

        List<CityCountry> cityCartagena = new ArrayList<>(Collections.singletonList(
                new CityCountry("Latin America (South/Central America)", "Colombia")
        ));

        List<CityCountry> cityChicago = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "Illinois")
        ));

        List<CityCountry> cityFernando = new ArrayList<>(Collections.singletonList(
                new CityCountry("Latin America (South/Central America)", "Pernambuco")
        ));

        List<CityCountry> cityFlorianópolis = new ArrayList<>(Collections.singletonList(
                new CityCountry("Latin America (South/Central America)", "Santa Catarina")
        ));

        List<CityCountry> cityLas = new ArrayList<>(Arrays.asList(
                new CityCountry("North America", "Arizona.Nevada")
        ));

        List<CityCountry> cityLos = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "California")
        ));

        List<CityCountry> cityManaus = new ArrayList<>(Collections.singletonList(
                new CityCountry("Latin America (South/Central America)", "Amazonas")
        ));

        List<CityCountry> cityMiami = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "Florida")
        ));

        List<CityCountry> cityMontevideo = new ArrayList<>(Collections.singletonList(
                new CityCountry("Latin America (South/Central America)", "Uruguay")
        ));

        List<CityCountry> cityNewYork = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "Connecticut.New York")
        ));

        List<CityCountry> cityOrlando = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "Florida")
        ));

        List<CityCountry> cityRio = new ArrayList<>(Collections.singletonList(
                new CityCountry("Latin America (South/Central America)", "Rio de Janeiro")
        ));

        List<CityCountry> citySalvador = new ArrayList<>(Collections.singletonList(
                new CityCountry("Latin America (South/Central America)", "Bahia")
        ));

        List<CityCountry> citySan = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "California")
        ));

        List<CityCountry> citySantiago = new ArrayList<>(Collections.singletonList(
                new CityCountry("Latin America (South/Central America)", "Chile")
        ));

        List<CityCountry> citySão = new ArrayList<>(Collections.singletonList(
                new CityCountry("Latin America (South/Central America)", "São Paulo")
        ));

        List<CityCountry> cityWash = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "District of Columbia.Virgina")
        ));

        //-- Region
        List<CityCountry> cityAfrica = new ArrayList<>(Collections.singletonList(
                new CityCountry("Africa/Middle East", VALUE_GEOGRAPHIC_REGION_ALL)
        ));

        List<CityCountry> cityAmeriaca = new ArrayList<>(Arrays.asList(
                new CityCountry("Caribbean/Bahamas/Mexico", "Mexico.Mexico City.Baja California Sur.Jalisco.Nuevo León.Quintana Roo"),
                new CityCountry("Latin America (South/Central America)", VALUE_GEOGRAPHIC_ALL_EXCEPT_CITY,
                        "Mexico.Mexico City.Baja California Sur.Jalisco.Nuevo León.Quintana Roo",
                        "Chile.Amazonas.Bahia.Buenos Aires.Ciudad Autónoma de Buenos Aires.Colombia.Pernambuco.Rio de Janeiro.Río Negro.Santa Catarina.São Paulo.Uruguay"),
                new CityCountry("North America", "Mexico")
        ));

        List<CityCountry> cityAsia = new ArrayList<>(Collections.singletonList(
                new CityCountry("Asia Pacific", VALUE_GEOGRAPHIC_REGION_ALL)
        ));

        List<CityCountry> cityCanada = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "British Columbia.Canada.Ontario.Quebec")
        ));

        List<CityCountry> cityCaribe = new ArrayList<>(Arrays.asList(
                new CityCountry("Caribbean", VALUE_GEOGRAPHIC_REGION_ALL),
                new CityCountry("Caribbean/Bahamas/Mexico",VALUE_GEOGRAPHIC_ALL_EXCEPT_CITY,
                        "",
                        "Mexico.Mexico City.Baja California Sur.Jalisco.Nuevo León.Quintana Roo")
        ));

        List<CityCountry> cityUS = new ArrayList<>(Collections.singletonList(
                new CityCountry("North America", "Delaware.Georgia.Hawaii.Lousiana.Maine.Massachusetts.New Hampshire.Rhode Island.Texas.Vermont.Washington.Wisconsin")
        ));

        List<CityCountry> cityEuropa = new ArrayList<>(Arrays.asList(
                new CityCountry("Europe", VALUE_GEOGRAPHIC_ALL_EXCEPT_CITY,
                        "France.United Kingdom.Italy",
                        "")
        ));
        //</editor-fold>

        cities = new LinkedHashMap<>();

        //<editor-fold desc="Config city name">
        final String cityNameBariloche = "Bariloche";
        final String cityNameBuenosAires = "Buenos Aires";
        final String cityNameCartagena = "Cartagena";
        final String cityNameChicago = "Chicago";
        final String cityNameFernandoDeNoronha = "Fernando de Noronha";
        final String cityNameFlorianopolis = "Florianópolis";
        final String cityNameLasVegas = "Las Vegas";
        final String cityNameLosAngeles = "Los Angeles";
        final String cityNameManaus = "Manaus";
        final String cityNameMiami = "Miami";
        final String cityNameMontevideo = "Montevidéu";
        final String cityNameNewYork = "New York";
        final String cityNameOrlando = "Orlando";
        final String cityNameRioDeJaneiro = "Rio de Janeiro";
        final String cityNameSalvador = "Salvador";
        final String cityNameSanFrancisco = "São Francisco";
        final String cityNameSantiago = "Santiago";
        final String cityNameSãoPaulo = "São Paulo";
        final String cityNameWashingtonDC = "Washington, D.C.";
        //Region
        final String cityNameAfricaEmiradosArabes = "África - Emirados Árabes";
        final String cityNameAmericaLatina = "América Latina";
        final String cityNameAsia = "Ásia";
        final String cityNameCanada = "Canadá";
        final String cityNameCaribe = "Caribe";
        final String cityNameEUA = "EUA";
        final String cityNameEuropa = "Europa";

        //Encode string city name
        final String cityEncodeNameAsia = "A&#769;sia";
        final String cityEncodeNameAmericaLatina = "Ame&#769;rica Latina";
        final String cityEncodeNameCanada = "Canada&#769;";
        final String cityEncodeNameFlorianopolis = "Florianópolis";
        final String cityEncodeNameSanFrancisco = "San Francisco";
        final String cityEncodeNameAfricaEmiradosArabe= "Africa - Emirados Arabes";


        //</editor-fold>

        cities.put(cityNameBariloche, new City(cityNameBariloche, false, cityBariloche, true, 6638,
                0 ,""));

        cities.put(cityNameBuenosAires, new City(cityNameBuenosAires, false, cityBuenos, true, 6651,
                0, ""));

        cities.put(cityNameCartagena, new City(cityNameCartagena, false, cityCartagena, true, 6643,
                0, ""));

        cities.put(cityNameChicago, new City(cityNameChicago, false, cityChicago, true, 6720,
                1,7177,7178,7179,7180,7181, ""));

        cities.put(cityNameFernandoDeNoronha, new City(cityNameFernandoDeNoronha, true, cityFernando, true, 6652,
                2,7033,7034,7035,7036,7037, ""));

        cities.put(cityNameFlorianopolis,new City(cityNameFlorianopolis, true, cityFlorianópolis, true, 6653,
                3,7038,7039,7040,7041,7042, cityEncodeNameFlorianopolis));

        cities.put(cityNameLasVegas,new City(cityNameLasVegas, false, cityLas, true, 6721,
                4,6990,6991,6992,6993,6994, ""));

        cities.put(cityNameLosAngeles, new City(cityNameLosAngeles, false, cityLos, true, 6722,
                5,7182,7183,7184,7185,7186, ""));

        cities.put(cityNameManaus,new City(cityNameManaus,true, cityManaus, true, 6654,
                6,7043,7044,7045,7046,7047, ""));

        cities.put(cityNameMiami,new City(cityNameMiami, false, cityMiami, true, 6723,
                7,7187,7188,7189,7190,7191, ""));

        cities.put(cityNameMontevideo,new City(cityNameMontevideo, false,cityMontevideo, true, 6644,
                0, ""));

        cities.put(cityNameNewYork, new City(cityNameNewYork, false, cityNewYork, true, 6724,
                8,7192,7193,7194,7195,7196, ""));

        cities.put(cityNameOrlando,new City(cityNameOrlando, false, cityOrlando, true, 6725,
                9,7197,7198,7199,7200,7201, ""));

        cities.put(cityNameRioDeJaneiro,new City(cityNameRioDeJaneiro, true, cityRio, true, 6656,
                10,7058,7059,7060,7061,7062, ""));

        cities.put(cityNameSalvador, new City(cityNameSalvador, true, citySalvador, true, 6657,
                11,7063,7064,7065,7066,7067, ""));

        cities.put(cityNameSanFrancisco,new City(cityNameSanFrancisco, false, citySan, true, 6719,
                12,7207,7208,7209,7210,7211, cityEncodeNameSanFrancisco));

        cities.put(cityNameSantiago,new City(cityNameSantiago,false, citySantiago, true, 6640,
                0, ""));

        cities.put(cityNameSãoPaulo,new City(cityNameSãoPaulo, true,citySão, true, 6658,
                13,7068,7069,7070,7071,7072, ""));

        cities.put(cityNameWashingtonDC, new City(cityNameWashingtonDC, false, cityWash, true, 6726,
                14,7212,7213,7214,7215,7216, ""));
        //Region
        cities.put(cityNameAfricaEmiradosArabes,new City(cityNameAfricaEmiradosArabes, false, cityAfrica, false, 7099, 0, cityEncodeNameAfricaEmiradosArabe));
        cities.put(cityNameAmericaLatina, new City(cityNameAmericaLatina, false, cityAmeriaca, false, 0, 0, cityEncodeNameAmericaLatina));
        cities.put(cityNameAsia,new City(cityNameAsia, false, cityAsia, false, 6966, 0, cityEncodeNameAsia));
        cities.put(cityNameCanada,new City(cityNameCanada, false, cityCanada, false, 7102, 0, cityEncodeNameCanada));
        cities.put(cityNameCaribe,new City(cityNameCaribe, false, cityCaribe, false, 7106, 0, ""));
        cities.put(cityNameEUA,new City(cityNameEUA, false, cityUS, false, 7104, 0, ""));
        cities.put(cityNameEuropa,new City(cityNameEuropa, false, cityEuropa, false, 7097, 0, ""));
    }

    public static void reset() {
        SELECTED_CITY = "";
    }

    public static boolean setSelectedCity(String cityName) {
        if (!TextUtils.isEmpty(cityName) && cities.get(cityName) != null) {
            SELECTED_CITY = cityName;
            return true;
        }//-- do nothing save select city
        return false;
    }

    /** check city name exist in array city */
    public static boolean isCityInApp(String cityName) {
        if (!TextUtils.isEmpty(cityName) && cities.get(cityName) != null) {
            return true;
        }
        return false;
    }


    public static int selectedCity() {
        int pos = 0;
        for (Map.Entry<String, City> entry : cities.entrySet()){
            if(SELECTED_CITY.equalsIgnoreCase(entry.getKey())){
                break;
            }
            pos++;
        }
        return pos;
    }

    public static boolean citySelected() {
        return (!TextUtils.isEmpty(SELECTED_CITY) && cities.get(SELECTED_CITY) != null);
    }

    public static int diningCode() {
        if(!citySelected()) return 0;
        return cities.get(SELECTED_CITY).diningCode;
    }

    public static int guideCode() {
        if(!citySelected()) return 0;
        return cities.get(SELECTED_CITY).cityGuideCode;
    }

    public static String cityName() {
        if(!citySelected()) return DEFAULT_CITY;
        return cities.get(SELECTED_CITY).name;
    }

    public static String cityNameDefaultEmpty() {
        if(!citySelected()) return "";
        return cities.get(SELECTED_CITY).name;
    }

    /** get city name encode on logic */
    public static String cityEncodeName() {
        if(!citySelected()) return "All";
        if (TextUtils.isEmpty(cities.get(SELECTED_CITY).encodeCityName)) {
            return cities.get(SELECTED_CITY).name;
        } else {
            return cities.get(SELECTED_CITY).encodeCityName;
        }
    }

    /** is value city or region */
    public static boolean isCity() {
        return citySelected() && cities.get(SELECTED_CITY).isCity;
    }

    /** is the city of America or Canada*/
    public static boolean isCityOfUS() {
        return citySelected() && cities.get(SELECTED_CITY).isCityOfUS;
    }

    public static  List<CityCountry> cityCountryList() {
        return cities.get(SELECTED_CITY).cityCountryList;
    }

    public static CityGuide cityGuide(int code) {
        return cityGuide.get(code);
        /*if(result == null) {
            throw new IllegalStateException("passed city code not found");
        }*/
    }

    /**
     * @param cityGuideCategoryIndex either position of spa | bar | shopping...
     * @return int value of
     */
    public static int specificCityGuideCode(int cityGuideCategoryIndex) {
        if(!citySelected())
            return -1;
        int cityCode = cities.get(SELECTED_CITY).cityGuideCode;
        if(cityCode == 0) return -1;
        CityGuide cityGuide = cityGuide(cityCode);
        if(cityGuide == null || cityGuide.size() <= cityGuideCategoryIndex) return -1;
        return cityGuide(cityCode).at(cityGuideCategoryIndex);
    }

    public static boolean isDiningItem(int subCategoryID) {
        for (Map.Entry<String, City> entry : cities.entrySet()){
            City city = entry.getValue();
            if(city.diningCode == subCategoryID) return true;
        }
        return false;
    }

    public static boolean isCityGuideItem(int catID) {
        return catID > 0 && cityGuideCodeList.contains(catID);
    }
}
