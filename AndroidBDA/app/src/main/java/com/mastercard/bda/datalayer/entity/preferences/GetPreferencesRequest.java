package com.mastercard.bda.datalayer.entity.preferences;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 7/27/2017.
 */

public class GetPreferencesRequest {

    @SerializedName("AccessToken")
    private final String accessToken;
    @SerializedName("ConsumerKey")
    private final String consumerKey;
    @SerializedName("Functionality")
    private final String functionality;
    @SerializedName("OnlineMemberId")
    private final String onlineMemberID;

    public GetPreferencesRequest(String accessToken, String onlineMemberID) {
        this.accessToken = accessToken;
        this.consumerKey = "DMA";
        this.functionality = "GetPreference";
        this.onlineMemberID = onlineMemberID;
    }

}
