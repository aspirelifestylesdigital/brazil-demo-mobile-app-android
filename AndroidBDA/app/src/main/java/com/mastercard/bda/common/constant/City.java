package com.mastercard.bda.common.constant;

import java.util.List;

/**
 * Created by vinh.trinh on 6/2/2017.
 */

public class City {
    public final String name;
    public List<CityCountry> cityCountryList;
    /** check is city or region*/
    public final boolean isCity;
    public final int diningCode;
    public final int cityGuideCode;
    /** the city of United State (American) and the region Canada*/
    public final boolean isCityOfUS;
    /** String encode city use for API*/
    public final String encodeCityName;



    public City(String name, boolean isCityOfUS, List<CityCountry> cityCountryList, boolean isCity, int diningCode, int cityGuideCode, int dining, int bar, int spa, int shopping, int culture, String encodeCityName) {
        this(name, isCityOfUS, cityCountryList, isCity, diningCode, cityGuideCode, encodeCityName);
        CityData.cityGuide.append(cityGuideCode, new CityGuide(dining, bar, spa, shopping, culture));
    }

    /*public City(String name, List<CityCountry> cityCountryList, boolean isCity, int diningCode, int cityGuideCode, int accommodation) {
        this(name, cityCountryList, isCity, diningCode, cityGuideCode);
        CityData.cityGuide.append(cityGuideCode, new CityGuide(accommodation));
    }*/

    public City(String name, boolean isCityOfUS, List<CityCountry> cityCountryList, boolean isCity, int diningCode, int cityGuideCode, String encodeCityName) {
        this.name = name;
        this.isCityOfUS = isCityOfUS;
        this.cityCountryList = cityCountryList;
        this.diningCode = diningCode;
        this.cityGuideCode = cityGuideCode;
        this.isCity = isCity;
        this.encodeCityName = encodeCityName;
    }
}
