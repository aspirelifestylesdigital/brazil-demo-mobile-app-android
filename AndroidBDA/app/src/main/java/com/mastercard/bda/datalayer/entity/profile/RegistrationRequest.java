package com.mastercard.bda.datalayer.entity.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mastercard.bda.datalayer.entity.Member;
import com.mastercard.bda.datalayer.entity.MemberDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinh.trinh on 6/19/2017.
 */

public class RegistrationRequest {
    @Expose
    @SerializedName("Member")
    private Member member;
    @Expose
    @SerializedName("MemberDetails")
    private List<MemberDetail> memberDetails;

    public RegistrationRequest(Member member, MemberDetail memberDetail) {
        this.member = member;
        this.memberDetails = new ArrayList<>();
        this.memberDetails.add(memberDetail);
    }
}