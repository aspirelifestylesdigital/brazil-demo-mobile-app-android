package com.mastercard.bda.common.constant;

public class CityCountry {
    private String subCategory;
    /** list city split with chars dot */
    private String geographic;

    /** geographic name of city except follow document */
    private String exceptCity;

    // mapping list city expect
    private String mappingCity;

    public CityCountry(String subCategory, String geographic) {
        this.subCategory = subCategory;
        this.geographic = geographic;
        this.mappingCity = "";
        this.exceptCity = "";
    }

    public CityCountry(String subCategory, String geographic, String mappingCity, String exceptCity) {
        this(subCategory,geographic);
        this.mappingCity = mappingCity;
        this.exceptCity = exceptCity;
    }

    public String getExceptCity() {
        return exceptCity;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public String getGeographic() {
        return geographic;
    }

    public String getMappingCity() {
        return mappingCity;
    }
}
