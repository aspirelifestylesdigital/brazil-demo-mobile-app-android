package com.api.aspire.data.entity.profile;

import com.google.gson.annotations.SerializedName;

public class ChangePasswordOKTAResponse{

	@SerializedName("provider")
	private Provider provider;

	public Provider getProvider() {
		return provider;
	}

	public boolean isSuccess() {
		return getProvider() != null && ("OKTA".equals(getProvider().type) && "OKTA".equals(getProvider().name));
	}

	class Provider{

		@SerializedName("name")
		private String name;

		@SerializedName("type")
		private String type;
	}
}