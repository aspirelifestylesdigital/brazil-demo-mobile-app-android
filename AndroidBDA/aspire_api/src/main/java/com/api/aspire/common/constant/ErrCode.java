package com.api.aspire.common.constant;

public enum ErrCode {
    CONNECTIVITY_PROBLEM,
    API_ERROR,
    UNKNOWN_ERROR,
    PROFILE_ERROR,
    PASS_CODE_ERROR,
    PASS_CODE_INVALID_ERROR,
    WRONG_PASS,
    USER_EXISTS_ERROR
}