package com.api.aspire.domain.repository;

import com.api.aspire.data.entity.profile.CreateProfileAspireRequest;
import com.api.aspire.domain.model.ProfileAspire;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface ProfileAspireRepository {

    Single<ProfileAspire> signInProfile(String accessToken, String password, String username);

    Single<ProfileAspire> signInNoSaveStorage(String accessToken, String password, String username);

    Single<ProfileAspire> saveProfileStorage(ProfileAspire profileAspire);

    Single<ProfileAspire> loadProfile(String accessToken);

    Completable saveProfile(String accessToken, ProfileAspire profile);

    Completable createProfile(String serviceToken,
                              CreateProfileAspireRequest createProfileRequest,
                              ProfileAspire profile);

}