package com.api.aspire.data.datasource;

import android.text.TextUtils;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.exception.BackendException;
import com.api.aspire.data.entity.profile.CreateProfileAspireRequest;
import com.api.aspire.data.entity.profile.CreateProfileAspireResponse;
import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.api.aspire.data.entity.profile.UpdateProfileAspireRequest;
import com.api.aspire.data.entity.profile.UpdateProfileAspireResponse;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.retro2client.AppHttpClient;
import com.api.aspire.domain.model.ProfileAspire;
import com.google.gson.Gson;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

public class RemoteProfileDataStoreAspire {

    private PreferencesStorageAspire preferencesStorageAspire;

    public RemoteProfileDataStoreAspire(PreferencesStorageAspire pref) {
        this.preferencesStorageAspire = pref;
    }

    public Single<ProfileAspire> loadProfile(String accessToken) {
        return Single.create(e -> {
            Call<ProfileAspireResponse> call = AppHttpClient.getInstance()
                    .getAspireProfileApi()
                    .retrieveProfile(accessToken);

            Response<ProfileAspireResponse> response = call.execute();
            if (!response.isSuccessful()) {
                e.onError(new BackendException(response.errorBody().string()));
                return;
            }

            ProfileAspireResponse responseBody = response.body();
            if (responseBody != null) {
                /*ProfileAspire profile = MapDataProfile.mapResponse(responseBody, secretKey, userNameEmail);

                //-- save select city local
                String nameCity = CityData.cityNameDefaultEmpty();
                preferencesStorageAspire.editor().selectedCity(nameCity).build().save();*/

                ProfileAspire profile = new ProfileAspire(new Gson().toJson(responseBody));
                e.onSuccess(profile);
            } else {
                e.onError(new Exception(response.message()));
            }
        });
    }

    public Completable saveProfile(String accessToken, UpdateProfileAspireRequest updateProfileRequest) {
        return Completable.create(e -> {

            Call<UpdateProfileAspireResponse> call = AppHttpClient.getInstance()
                    .getAspireProfileApi()
                    .updateProfile(accessToken, updateProfileRequest);

            Response<UpdateProfileAspireResponse> response = call.execute();
            if (!response.isSuccessful()) {
                e.onError(new BackendException(response.errorBody().string()));
                return;
            }

            UpdateProfileAspireResponse updateProfileAspireResponse = response.body();
            if (updateProfileAspireResponse != null && updateProfileAspireResponse.isSuccess()) {
                e.onComplete();
            } else {
                e.onError(new Exception(response.message()));
            }
        });
    }

    public Completable createProfile(String serviceToken, CreateProfileAspireRequest createProfileRequest) {
        return Completable.create(e -> {

            Call<CreateProfileAspireResponse> call = AppHttpClient.getInstance()
                    .getAspireCreateProfileApi()
                    .createCustomer(serviceToken, createProfileRequest);

            Response<CreateProfileAspireResponse> response = call.execute();
            if (!response.isSuccessful()) {

                if (response.code() == 400 && isExistsUser(response.errorBody().string())) {
                    e.onError(new BackendException(ErrCode.USER_EXISTS_ERROR.name()));
                } else {
                    e.onError(new Exception(response.errorBody().string()));
                }
                return;
            }

            CreateProfileAspireResponse responseBody = response.body();
            final String repSuccess = "The new customer account is registered successfully";
            if (responseBody != null && repSuccess.equalsIgnoreCase(responseBody.message)) {
                e.onComplete();
            } else {
                e.onError(new Exception(response.message()));
            }
        });
    }

    private boolean isExistsUser(String errorMessage) {
        final String accountExists = "An object with this field already exists";

        return !TextUtils.isEmpty(errorMessage) && (errorMessage.contains(accountExists)
                || errorMessage.contains("ACE_BADREQUEST_008")
                || errorMessage.contains("ACE_BADREQUEST_016"));
    }
}
