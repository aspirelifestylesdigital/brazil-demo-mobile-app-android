package com.api.aspire.domain.usecases;

import android.content.Context;

import com.api.aspire.data.entity.profile.CreateProfileAspireRequest;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.ProfileAspireRepository;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * flow input passCode before create user
 */
public class CreateProfileCase extends UseCase<Void, CreateProfileCase.Params> {

    private ProfileAspireRepository profileRepository;
    private GetToken getToken;
    // just for location setting, all of craps below this line appear
    private LoadProfile loadProfile;
    private GetAccessToken getAccessToken;

    public CreateProfileCase(Context context, MapProfileBase mapLogic) {
        final PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(context);
        final ProfileDataAspireRepository profileDataRepository = new ProfileDataAspireRepository(preferencesStorage, mapLogic);
        this.getToken = new GetToken(context);
        this.loadProfile = new LoadProfile(context, mapLogic);
        this.getAccessToken = new GetAccessToken(context, mapLogic);
        this.profileRepository = profileDataRepository;
    }

    @Override
    Observable<Void> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    Single<Void> buildUseCaseSingle(Params params) {
        return null;
    }

    @Override
    public Completable buildUseCaseCompletable(Params params) {
        //set password in profile
//        params.profile.setSecretKey(params.secretKey);
//
//        //passCode empty -> no happen this case because (get passCode from splash)
//        return checkPassCode.passCodeStorage()
//                .flatMapCompletable(passCode -> {
//                    //-- add passCode from local
//                    params.passCode = passCode;
//
//                });
        return initProfile(params);
    }

    public Completable initProfile(Params params) {
        return getToken.getTokenService(params.defaultUserName, params.defaultSecretKey)
                .flatMapCompletable(serviceToken -> createProfile(serviceToken, params))
                .andThen(getAccessToken.getTokenSignIn()
                        .flatMap(tokenSignIn -> loadProfile.buildUseCaseSingle(tokenSignIn))
                        .flatMapCompletable(profileLocal -> Completable.create(CompletableEmitter::onComplete))
                );
    }

    private Completable createProfile(String serviceToken, Params params) {
        return profileRepository.createProfile(serviceToken, params.createProfileRequest, params.profile);
    }

    public static class Params {
        public final ProfileAspire profile;
        public final CreateProfileAspireRequest createProfileRequest;
        public final String defaultUserName;
        public final String defaultSecretKey;

        public Params(ProfileAspire profile,
                      CreateProfileAspireRequest createProfileRequest,
                      String defaultUserName,
                      String defaultSecretKey) {
            this.profile = profile;
            this.createProfileRequest = createProfileRequest;
            this.defaultUserName = defaultUserName;
            this.defaultSecretKey = defaultSecretKey;
        }
    }

}
