package com.api.aspire.common.constant;


import android.text.TextUtils;

import static com.api.aspire.common.constant.ErrCode.PROFILE_ERROR;

public final class ErrorApi {

    public static boolean isTokenExpired(int responseCode, String errorMessage) {
        return responseCode == 401 && errorMessage.contains("Access token is either invalid or has expired");
    }

    /**
     * Error 1: {"error":"invalid_grant","error_description":"The credentials provided were invalid."}
     * Error 2:
     * {
     * "errorCode": "ACE_BADREQUEST_000",
     * "message": "Bad request exception at PMA API"
     * }
     */
    public static boolean isGetTokenError(String errorMessage) {
        return (!TextUtils.isEmpty(errorMessage) &&
                (errorMessage.contains("The credentials provided were invalid.")
                        || errorMessage.contains("Bad request exception at PMA API")
                        || PROFILE_ERROR.name().equalsIgnoreCase(errorMessage)
                )
        );
    }

    public static boolean isPasswordWrong(String errorMessage){
        return (!TextUtils.isEmpty(errorMessage) &&
                ( errorMessage.contains("E0000014")
                        || errorMessage.contains("Password has been used too recently")
                        || errorMessage.contains("Old Password is not correct")
                        || errorMessage.contains("Password cannot be your current password")
                )
        );
    }

}
