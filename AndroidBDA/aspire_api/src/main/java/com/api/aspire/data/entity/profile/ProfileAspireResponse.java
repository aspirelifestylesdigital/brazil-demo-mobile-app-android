package com.api.aspire.data.entity.profile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileAspireResponse {
//    @Expose
//    @SerializedName("memberships")
//    protected List<Memberships> memberships;
    @Expose
    @SerializedName("preferences")
    protected List<Preferences> preferences;
    @Expose
    @SerializedName("emails")
    protected List<Emails> emails;
    @Expose
    @SerializedName("relationships")
    protected List<Relationships> relationships;
    @Expose
    @SerializedName("phones")
    protected List<Phones> phones;
    @Expose
    @SerializedName("locations")
    protected List<Locations> locations;
    @Expose
    @SerializedName("personType")
    protected String persontype;
    @Expose
    @SerializedName("disabilityStatus")
    protected String disabilitystatus;
    @Expose
    @SerializedName("homeCountry")
    protected String homecountry;
    @Expose
    @SerializedName("recordEffectiveTo")
    protected String recordeffectiveto;
    @Expose
    @SerializedName("recordEffectiveFrom")
    protected String recordeffectivefrom;
    @Expose
    @SerializedName("salutation")
    protected String salutation;
    @Expose
    @SerializedName("prefix")
    protected String prefix;
    @Expose
    @SerializedName("displayName")
    protected String displayname;
    @Expose
    @SerializedName("lastName")
    protected String lastname;
    @Expose
    @SerializedName("firstName")
    protected String firstname;
    @Expose
    @SerializedName("partyId")
    protected String partyid;

    @Expose
    @SerializedName("appUserPreferences")
    public List<AppUserPreferences> appUserPreferences;

    public List<AppUserPreferences> getAppUserPreferences() {
        return appUserPreferences;
    }

    public void setAppUserPreferences(List<AppUserPreferences> appUserPreferences) {
        this.appUserPreferences = appUserPreferences;
    }

    public List<Relationships> getRelationships() {
        return relationships;
    }

    public void setRelationships(List<Relationships> relationships) {
        this.relationships = relationships;
    }

//    public List<Memberships> getMemberships() {
//        return memberships;
//    }
//
//    public void setMemberships(List<Memberships> memberships) {
//        this.memberships = memberships;
//    }

    public List<Preferences> getPreferences() {
        return preferences;
    }

    public void setPreferences(List<Preferences> preferences) {
        this.preferences = preferences;
    }

    public List<Emails> getEmails() {
        return emails;
    }

    public void setEmails(List<Emails> emails) {
        this.emails = emails;
    }

    public List<Phones> getPhones() {
        return phones;
    }

    public void setPhones(List<Phones> phones) {
        this.phones = phones;
    }

    public List<Locations> getLocations() {
        return locations;
    }

    public void setLocations(List<Locations> locations) {
        this.locations = locations;
    }

    public String getPersontype() {
        return persontype;
    }

    public void setPersontype(String persontype) {
        this.persontype = persontype;
    }

    public String getDisabilitystatus() {
        return disabilitystatus;
    }

    public void setDisabilitystatus(String disabilitystatus) {
        this.disabilitystatus = disabilitystatus;
    }

    public String getHomecountry() {
        return homecountry;
    }

    public void setHomecountry(String homecountry) {
        this.homecountry = homecountry;
    }

    public String getRecordeffectiveto() {
        return recordeffectiveto;
    }

    public void setRecordeffectiveto(String recordeffectiveto) {
        this.recordeffectiveto = recordeffectiveto;
    }

    public String getRecordeffectivefrom() {
        return recordeffectivefrom;
    }

    public void setRecordeffectivefrom(String recordeffectivefrom) {
        this.recordeffectivefrom = recordeffectivefrom;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPartyid() {
        return partyid;
    }

    public void setPartyid(String partyid) {
        this.partyid = partyid;
    }

    public static class Relationships implements Parcelable {
        @Expose
        @SerializedName("partyName")
        private String partyname;
        @Expose
        @SerializedName("partyType")
        private String partytype;
        @Expose
        @SerializedName("partyId")
        private String partyid;
        @Expose
        @SerializedName("relationshipType")
        private String relationshiptype;
        @Expose
        @SerializedName("relationshipId")
        private String relationshipid;

        public String getPartyname() {
            return partyname;
        }

        public void setPartyname(String partyname) {
            this.partyname = partyname;
        }

        public String getPartytype() {
            return partytype;
        }

        public void setPartytype(String partytype) {
            this.partytype = partytype;
        }

        public String getPartyid() {
            return partyid;
        }

        public void setPartyid(String partyid) {
            this.partyid = partyid;
        }

        public String getRelationshiptype() {
            return relationshiptype;
        }

        public void setRelationshiptype(String relationshiptype) {
            this.relationshiptype = relationshiptype;
        }

        public String getRelationshipid() {
            return relationshipid;
        }

        public void setRelationshipid(String relationshipid) {
            this.relationshipid = relationshipid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.partyname);
            dest.writeString(this.partytype);
            dest.writeString(this.partyid);
            dest.writeString(this.relationshiptype);
            dest.writeString(this.relationshipid);
        }

        public Relationships() {
        }

        protected Relationships(Parcel in) {
            this.partyname = in.readString();
            this.partytype = in.readString();
            this.partyid = in.readString();
            this.relationshiptype = in.readString();
            this.relationshipid = in.readString();
        }

        public static final Creator<Relationships> CREATOR = new Creator<Relationships>() {
            @Override
            public Relationships createFromParcel(Parcel source) {
                return new Relationships(source);
            }

            @Override
            public Relationships[] newArray(int size) {
                return new Relationships[size];
            }
        };
    }

//    public static class Memberships implements Parcelable {
//        @Expose
//        @SerializedName("referenceName")
//        private String referencename;
//        @Expose
//        @SerializedName("programName")
//        private String programname;
//        @Expose
//        @SerializedName("membershipCategory")
//        private String membershipcategory;
//        @Expose
//        @SerializedName("membershipId")
//        private String membershipid;
//
//        public String getReferencename() {
//            return referencename;
//        }
//
//        public void setReferencename(String referencename) {
//            this.referencename = referencename;
//        }
//
//        public String getProgramname() {
//            return programname;
//        }
//
//        public void setProgramname(String programname) {
//            this.programname = programname;
//        }
//
//        public String getMembershipcategory() {
//            return membershipcategory;
//        }
//
//        public void setMembershipcategory(String membershipcategory) {
//            this.membershipcategory = membershipcategory;
//        }
//
//        public String getMembershipid() {
//            return membershipid;
//        }
//
//        public void setMembershipid(String membershipid) {
//            this.membershipid = membershipid;
//        }
//
//        public Memberships(String referenceName, String membershipCategory) {
//            this.referencename = referenceName;
//            this.membershipcategory = membershipCategory;
//        }
//
//        @Override
//        public int describeContents() {
//            return 0;
//        }
//
//        @Override
//        public void writeToParcel(Parcel dest, int flags) {
//            dest.writeString(this.referencename);
//            dest.writeString(this.programname);
//            dest.writeString(this.membershipcategory);
//            dest.writeString(this.membershipid);
//        }
//
//        protected Memberships(Parcel in) {
//            this.referencename = in.readString();
//            this.programname = in.readString();
//            this.membershipcategory = in.readString();
//            this.membershipid = in.readString();
//        }
//
//        public static final Creator<Memberships> CREATOR = new Creator<Memberships>() {
//            @Override
//            public Memberships createFromParcel(Parcel source) {
//                return new Memberships(source);
//            }
//
//            @Override
//            public Memberships[] newArray(int size) {
//                return new Memberships[size];
//            }
//        };
//    }

    public static class Preferences implements Parcelable {
        @Expose
        @SerializedName("preferenceValue")
        private String preferencevalue;
        @Expose
        @SerializedName("preferenceType")
        private String preferencetype;
        @Expose
        @SerializedName("preferenceId")
        private String preferenceid;

        public String getPreferencevalue() {
            return preferencevalue;
        }

        public void setPreferencevalue(String preferencevalue) {
            this.preferencevalue = preferencevalue;
        }

        public String getPreferencetype() {
            return preferencetype;
        }

        public void setPreferencetype(String preferencetype) {
            this.preferencetype = preferencetype;
        }

        public String getPreferenceid() {
            return preferenceid;
        }

        public void setPreferenceid(String preferenceid) {
            this.preferenceid = preferenceid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.preferencevalue);
            dest.writeString(this.preferencetype);
            dest.writeString(this.preferenceid);
        }

        /**
         * use for init new preference
         */
        public Preferences(String preferenceValue, String preferenceType) {
            this.preferencevalue = preferenceValue;
            this.preferencetype = preferenceType;
            this.preferenceid = null;
        }

        protected Preferences(Parcel in) {
            this.preferencevalue = in.readString();
            this.preferencetype = in.readString();
            this.preferenceid = in.readString();
        }

        public static final Creator<Preferences> CREATOR = new Creator<Preferences>() {
            @Override
            public Preferences createFromParcel(Parcel source) {
                return new Preferences(source);
            }

            @Override
            public Preferences[] newArray(int size) {
                return new Preferences[size];
            }
        };
    }

    public static class Socialmedias implements Parcelable {
        @Expose
        @SerializedName("pageType")
        private String pagetype;
        @Expose
        @SerializedName("url")
        private String url;
        @Expose
        @SerializedName("webpage")
        private String webpage;
        @Expose
        @SerializedName("socialMediaId")
        private String socialmediaid;

        public String getPagetype() {
            return pagetype;
        }

        public void setPagetype(String pagetype) {
            this.pagetype = pagetype;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getWebpage() {
            return webpage;
        }

        public void setWebpage(String webpage) {
            this.webpage = webpage;
        }

        public String getSocialmediaid() {
            return socialmediaid;
        }

        public void setSocialmediaid(String socialmediaid) {
            this.socialmediaid = socialmediaid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.pagetype);
            dest.writeString(this.url);
            dest.writeString(this.webpage);
            dest.writeString(this.socialmediaid);
        }

        public Socialmedias() {
        }

        protected Socialmedias(Parcel in) {
            this.pagetype = in.readString();
            this.url = in.readString();
            this.webpage = in.readString();
            this.socialmediaid = in.readString();
        }

        public static final Creator<Socialmedias> CREATOR = new Creator<Socialmedias>() {
            @Override
            public Socialmedias createFromParcel(Parcel source) {
                return new Socialmedias(source);
            }

            @Override
            public Socialmedias[] newArray(int size) {
                return new Socialmedias[size];
            }
        };
    }

    public static class Websites implements Parcelable {
        @Expose
        @SerializedName("webURL")
        private String weburl;
        @Expose
        @SerializedName("websiteType")
        private String websitetype;
        @Expose
        @SerializedName("websiteId")
        private String websiteid;

        public String getWeburl() {
            return weburl;
        }

        public void setWeburl(String weburl) {
            this.weburl = weburl;
        }

        public String getWebsitetype() {
            return websitetype;
        }

        public void setWebsitetype(String websitetype) {
            this.websitetype = websitetype;
        }

        public String getWebsiteid() {
            return websiteid;
        }

        public void setWebsiteid(String websiteid) {
            this.websiteid = websiteid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.weburl);
            dest.writeString(this.websitetype);
            dest.writeString(this.websiteid);
        }

        public Websites() {
        }

        protected Websites(Parcel in) {
            this.weburl = in.readString();
            this.websitetype = in.readString();
            this.websiteid = in.readString();
        }

        public static final Creator<Websites> CREATOR = new Creator<Websites>() {
            @Override
            public Websites createFromParcel(Parcel source) {
                return new Websites(source);
            }

            @Override
            public Websites[] newArray(int size) {
                return new Websites[size];
            }
        };
    }

    public static class Faxes implements Parcelable {
        @Expose
        @SerializedName("faxNumber")
        private String faxnumber;
        @Expose
        @SerializedName("countryCode")
        private String countrycode;
        @Expose
        @SerializedName("faxId")
        private String faxid;

        public String getFaxnumber() {
            return faxnumber;
        }

        public void setFaxnumber(String faxnumber) {
            this.faxnumber = faxnumber;
        }

        public String getCountrycode() {
            return countrycode;
        }

        public void setCountrycode(String countrycode) {
            this.countrycode = countrycode;
        }

        public String getFaxid() {
            return faxid;
        }

        public void setFaxid(String faxid) {
            this.faxid = faxid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.faxnumber);
            dest.writeString(this.countrycode);
            dest.writeString(this.faxid);
        }

        public Faxes() {
        }

        protected Faxes(Parcel in) {
            this.faxnumber = in.readString();
            this.countrycode = in.readString();
            this.faxid = in.readString();
        }

        public static final Creator<Faxes> CREATOR = new Creator<Faxes>() {
            @Override
            public Faxes createFromParcel(Parcel source) {
                return new Faxes(source);
            }

            @Override
            public Faxes[] newArray(int size) {
                return new Faxes[size];
            }
        };
    }

    public static class Emails implements Parcelable {
        @Expose
        @SerializedName("emailAddress")
        private String emailaddress;
        @Expose
        @SerializedName("emailType")
        private String emailtype;
        @Expose
        @SerializedName("emailId")
        private String emailid;

        public String getEmailaddress() {
            return emailaddress;
        }

        public void setEmailaddress(String emailaddress) {
            this.emailaddress = emailaddress;
        }

        public String getEmailtype() {
            return emailtype;
        }

        public void setEmailtype(String emailtype) {
            this.emailtype = emailtype;
        }

        public String getEmailid() {
            return emailid;
        }

        public void setEmailid(String emailid) {
            this.emailid = emailid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.emailaddress);
            dest.writeString(this.emailtype);
            dest.writeString(this.emailid);
        }

        public Emails() {
        }

        protected Emails(Parcel in) {
            this.emailaddress = in.readString();
            this.emailtype = in.readString();
            this.emailid = in.readString();
        }

        public static final Creator<Emails> CREATOR = new Creator<Emails>() {
            @Override
            public Emails createFromParcel(Parcel source) {
                return new Emails(source);
            }

            @Override
            public Emails[] newArray(int size) {
                return new Emails[size];
            }
        };
    }

    public static class Phones implements Parcelable {
        @Expose
        @SerializedName("phoneNumber")
        private String phonenumber;
        @Expose
        @SerializedName("phoneType")
        private String phonetype;
        @Expose
        @SerializedName("phoneId")
        private String phoneid;

        public String getPhonenumber() {
            return phonenumber;
        }

        public void setPhonenumber(String phonenumber) {
            this.phonenumber = phonenumber;
        }


        public String getPhonetype() {
            return phonetype;
        }

        public void setPhonetype(String phonetype) {
            this.phonetype = phonetype;
        }

        public String getPhoneid() {
            return phoneid;
        }

        public void setPhoneid(String phoneid) {
            this.phoneid = phoneid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.phonenumber);
            dest.writeString(this.phonetype);
            dest.writeString(this.phoneid);
        }

        public Phones() {
        }

        protected Phones(Parcel in) {
            this.phonenumber = in.readString();
            this.phonetype = in.readString();
            this.phoneid = in.readString();
        }

        public static final Creator<Phones> CREATOR = new Creator<Phones>() {
            @Override
            public Phones createFromParcel(Parcel source) {
                return new Phones(source);
            }

            @Override
            public Phones[] newArray(int size) {
                return new Phones[size];
            }
        };
    }

    public static class Locations implements Parcelable {
        @Expose
        @SerializedName("address")
        private ProfileAspireResponse.Address address;
        @Expose
        @SerializedName("locationType")
        private String locationtype;
        @Expose
        @SerializedName("locationId")
        private String locationid;

        public Address getAddress() {
            return address;
        }

        public void setAddress(Address address) {
            this.address = address;
        }

        public String getLocationtype() {
            return locationtype;
        }

        public void setLocationtype(String locationtype) {
            this.locationtype = locationtype;
        }

        public String getLocationid() {
            return locationid;
        }

        public void setLocationid(String locationid) {
            this.locationid = locationid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.address, flags);
            dest.writeString(this.locationtype);
            dest.writeString(this.locationid);
        }

        public Locations(Address address, String locationtype) {
            this.address = address;
            this.locationtype = locationtype;
        }

        public Locations(Address address, String locationtype, String locationid) {
            this.address = address;
            this.locationtype = locationtype;
            this.locationid = locationid;
        }

        protected Locations(Parcel in) {
            this.address = in.readParcelable(Address.class.getClassLoader());
            this.locationtype = in.readString();
            this.locationid = in.readString();
        }

        public static final Creator<Locations> CREATOR = new Creator<Locations>() {
            @Override
            public Locations createFromParcel(Parcel source) {
                return new Locations(source);
            }

            @Override
            public Locations[] newArray(int size) {
                return new Locations[size];
            }
        };
    }

    public static class Address implements Parcelable {
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("city")
        private String city;
        @Expose
        @SerializedName("addressLine5")
        private String addressline5;
        @Expose
        @SerializedName("addressLine1")
        private String addressline1;

        @SerializedName("zipCode")
        private String zipCode;

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAddressline5() {
            return addressline5;
        }

        public void setAddressline5(String addressline5) {
            this.addressline5 = addressline5;
        }

        public String getAddressline1() {
            return addressline1;
        }

        public void setAddressline1(String addressline1) {
            this.addressline1 = addressline1;
        }

        public String getZipCode() {
            return zipCode;
        }

        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.country);
            dest.writeString(this.city);
            dest.writeString(this.addressline5);
            dest.writeString(this.addressline1);
        }

        public Address(String country, String city, String addressline5) {
            this.country = country;
            this.city = city;
            this.addressline5 = addressline5;
        }

        protected Address(Parcel in) {
            this.country = in.readString();
            this.city = in.readString();
            this.addressline5 = in.readString();
            this.addressline1 = in.readString();
        }

        public static final Creator<Address> CREATOR = new Creator<Address>() {
            @Override
            public Address createFromParcel(Parcel source) {
                return new Address(source);
            }

            @Override
            public Address[] newArray(int size) {
                return new Address[size];
            }
        };
    }

    public static class AppUserPreferences implements Parcelable {
        @Expose
        @SerializedName("appUserPreferenceId")
        private String appUserPreferenceId;
        @Expose
        @SerializedName("preferenceKey")
        private String preferenceKey;
        @Expose
        @SerializedName("preferenceValue")
        private String preferenceValue;

        public String getAppUserPreferenceId() {
            return appUserPreferenceId;
        }

        public void setAppUserPreferenceId(String appUserPreferenceId) {
            this.appUserPreferenceId = appUserPreferenceId;
        }

        public String getPreferenceKey() {
            return preferenceKey;
        }

        public void setPreferenceKey(String preferenceKey) {
            this.preferenceKey = preferenceKey;
        }

        public String getPreferenceValue() {
            return preferenceValue;
        }

        public void setPreferenceValue(String preferenceValue) {
            this.preferenceValue = preferenceValue;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.appUserPreferenceId);
            dest.writeString(this.preferenceKey);
            dest.writeString(this.preferenceValue);
        }

        public AppUserPreferences() {
        }

        public AppUserPreferences(String preferenceKey, String preferenceValue) {
            this.preferenceKey = preferenceKey;
            this.preferenceValue = preferenceValue;
        }

        protected AppUserPreferences(Parcel in) {
            this.appUserPreferenceId = in.readString();
            this.preferenceKey = in.readString();
            this.preferenceValue = in.readString();
        }

        public static final Parcelable.Creator<AppUserPreferences> CREATOR = new Parcelable.Creator<AppUserPreferences>() {
            @Override
            public AppUserPreferences createFromParcel(Parcel source) {
                return new AppUserPreferences(source);
            }

            @Override
            public AppUserPreferences[] newArray(int size) {
                return new AppUserPreferences[size];
            }
        };
    }

}
