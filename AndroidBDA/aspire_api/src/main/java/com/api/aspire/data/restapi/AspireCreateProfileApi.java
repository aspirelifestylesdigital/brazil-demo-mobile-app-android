package com.api.aspire.data.restapi;


import com.api.aspire.data.entity.passcode.PassCodeRequest;
import com.api.aspire.data.entity.passcode.PassCodeResponse;
import com.api.aspire.data.entity.profile.CreateProfileAspireRequest;
import com.api.aspire.data.entity.profile.CreateProfileAspireResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface AspireCreateProfileApi {

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST(".")
    Call<CreateProfileAspireResponse> createCustomer(@Header("Authorization") String auth,
                                                     @Body CreateProfileAspireRequest profileResponse);

}
