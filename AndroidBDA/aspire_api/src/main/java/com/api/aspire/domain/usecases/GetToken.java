package com.api.aspire.domain.usecases;

import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.common.exception.BackendException;
import com.api.aspire.data.entity.oauth.GetTokenAspireRequest;
import com.api.aspire.data.entity.oauth.GetTokenAspireResponse;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.restapi.AspireOAuthApi;
import com.api.aspire.data.retro2client.AppHttpClient;
import com.api.aspire.domain.model.AuthData;
import com.google.gson.Gson;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

public class GetToken {

    private PreferencesStorageAspire preferencesStorage;

    public GetToken(Context c) {
        this.preferencesStorage = new PreferencesStorageAspire(c);
    }

    public Single<String> getTokenUser(final String username, final String secretKey) {
        return getTokenUser(username, secretKey, false);
    }

    private Single<String> getTokenUser(final String username, final String secretKey, boolean isDoubleRequest) {
        return Single.create(e -> {
            AspireOAuthApi aspireOAuthApi = AppHttpClient.getInstance().getAspireOAuthApi();
            // access token
            GetTokenAspireRequest rq = new GetTokenAspireRequest(username, secretKey);

            Call<GetTokenAspireResponse> requestTokenCall = aspireOAuthApi.getTokenUser(rq.getGrantType(),
                    rq.getRedirectUri(),
                    rq.getScope(),
                    rq.getUsername(),
                    rq.getSecretKey()
            );
            Response<GetTokenAspireResponse> requestTokenResponse = requestTokenCall.execute();

            if (!requestTokenResponse.isSuccessful()) {
                if (isDoubleRequest) {
                    requestTokenResponse = requestTokenCall.clone().execute();
                    if (!requestTokenResponse.isSuccessful()) {
                        e.onError(new BackendException(requestTokenResponse.errorBody().string()));
                        return;
                    }//-- else continues handle accessToken

                } else {
                    e.onError(new BackendException(requestTokenResponse.errorBody().string()));
                    return;
                }
            }//-- else continues handle accessToken

            String accessToken = requestTokenResponse.body().getAccessToken();
            String tokenType = requestTokenResponse.body().getTokenType();

            if (TextUtils.isEmpty(accessToken) || TextUtils.isEmpty(tokenType)) {
                e.onError(new Exception(requestTokenResponse.body().toString()));
            } else {
                String token = tokenType + " " + accessToken;
                saveStorageAccessToken(token);
                e.onSuccess(token);
            }
        });
    }

    /**
     * Create new account
     * Check PassCode
     */
    public Single<String> getTokenService(final String usename, final String secretkey) {
        return Single.create(e -> {
            AspireOAuthApi aspireOAuthApi = AppHttpClient.getInstance().getAspireOAuthApi();
            // access token
            GetTokenAspireRequest rq = new GetTokenAspireRequest(usename, secretkey);
            Call<GetTokenAspireResponse> requestTokenCall = aspireOAuthApi.getTokenService(rq.getGrantType(),
                    rq.getRedirectUri(),
                    rq.getScope(),
                    rq.getUsername(),
                    rq.getSecretKey()
            );
            Response<GetTokenAspireResponse> requestTokenResponse = requestTokenCall.execute();

            if (!requestTokenResponse.isSuccessful()) {
                e.onError(new BackendException(requestTokenResponse.errorBody().string()));
                return;
            }

            String accessToken = requestTokenResponse.body().getAccessToken();
            String tokenType = requestTokenResponse.body().getTokenType();

            if (TextUtils.isEmpty(accessToken) || TextUtils.isEmpty(tokenType)) {
                e.onError(new Exception(requestTokenResponse.body().toString()));
            } else {
                String token = tokenType + " " + accessToken;
                e.onSuccess(token);
            }
        });
    }

    /**
     * save access token in storage device
     */
    private void saveStorageAccessToken(String accessToken) {
        long timeLimit = 30 * 60000L; //-- 30 minutes access token is expire
        Long timeExpiry = System.currentTimeMillis() + timeLimit;
        AuthData authData = new AuthData(accessToken, timeExpiry);

        String authObj = new Gson().toJson(authData);
        preferencesStorage.editor().authToken(authObj).build().save();
    }

    /**
     * @return true: token is Expired, otherwise false: token useful
     */
    private boolean isTokenExpired() {

        AuthData authData = preferencesStorage.authToken();
        if (authData == null || TextUtils.isEmpty(authData.accessToken)) {
            return true;
        }

        long currentTime = System.currentTimeMillis() - authData.timeExpireToken;
        return currentTime > 0; //-- time limit > time current
    }

    public Single<String> refreshToken(final String username, final String secretKey) {
        if (isTokenExpired()) {
            return getTokenUser(username, secretKey, true);
        } else {
            return Single.just(preferencesStorage.authToken().accessToken);
        }
    }

}