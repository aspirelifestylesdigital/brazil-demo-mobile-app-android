package com.api.aspire.domain.repository;

import io.reactivex.Single;

public interface AuthAspireRepository {

    Single<Boolean> checkPassCode(String tokenService, int passCode);
}
