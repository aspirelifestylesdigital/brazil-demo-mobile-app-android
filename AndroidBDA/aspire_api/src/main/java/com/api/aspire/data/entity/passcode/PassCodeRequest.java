package com.api.aspire.data.entity.passcode;

import com.api.aspire.data.entity.profile.VerificationMetadata;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassCodeRequest {
    @SerializedName("verificationMetadata")
    @Expose
    private VerificationMetadata verificationMetadata;

    public PassCodeRequest(int bin) {
        this.verificationMetadata = new VerificationMetadata(bin);
    }
}
